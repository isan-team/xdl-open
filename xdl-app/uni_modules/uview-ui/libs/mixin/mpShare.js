module.exports = {
	onLoad() {
		// 设置默认的转发参数
		uni.$u.mpShare = {
			title: '新动力提供各类赛车体验、会员服务、团队活动、性能测试、赛事活动、驾控培训、赛车旅游、赛道设计、品牌加盟等相关产品及服务。', // 默认为小程序名称
			path: 'pages/index/index', // 默认为当前页面路径
			imageUrl: 'https://xdlapp-bucket.oss-cn-hangzhou.aliyuncs.com/xdl/sharebg.jpeg' // 默认为当前页面的截图
		}


		//白名单
		const urlList = [
			'pages/index/index',
			'pages/activity/components/detail'
		]
		//获取路由信息
		const pages = getCurrentPages()
		//获取当前路由
		let nowPage = pages[pages.length - 1]
		//判断路由包含有白名单的路由就禁用
		if (!urlList.includes(nowPage.route)) {
			uni.hideShareMenu()
		}

	},
	onShareAppMessage() {
		return uni.$u.mpShare
	},
	onShareTimeline() {
		return uni.$u.mpShare
	}
}
