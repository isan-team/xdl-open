import BaseConfig from './config'
import cache from './cache'

module.exports = {
	post: function(url, data, header) {
		header = header || "application/json;charset=UTF-8";
		url = BaseConfig.apiUrl+url;
		//let token=uni.getStorageSync('token');
		let token = cache.get('token', null);
		return new Promise((succ, error) => {
			uni.request({
				url: url,
				data: data,
				method: "POST",
				header: {
					"content-type": header,
					"token":token
				},
				success: function(result) {
					if(result.data.code===40029){
						uni.showToast({
							icon: 'error',
							position: 'bottom',
							title: '请登陆小程序'
						});
					}else{
						succ.call(self, result.data)
					}
				},
				fail: function(e) {
					error.call(self, e);
					console.log('waring',e)
				}
			})
		})
	},
	get: function(url, data, header) {
		header = header || "application/json;charset=UTF-8";
		url = BaseConfig.apiUrl+url;
		//let token=uni.getStorageSync('token');
		let token = cache.get('token', null);
		return new Promise((succ, error) => {
			console.log('token',token)
			uni.request({
				url: url,
				data: data,
				method: "GET",
				header: {
					"content-type": header,
					"token":token
				},
				success: function(result) {
					if(result.data.code===40029){
						uni.showToast({
							icon: 'error',
							position: 'bottom',
							title: '请登陆小程序'
						});
					}else{
						succ.call(self, result.data)
					}
				},
				fail: function(e) {
					error.call(self, e)
					console.log('waring',e)
				}
			})
		})
	}
}
