let devEnv  = 'http://localhost:8080'
let prodEnv  = 'http://localhost:8080'
const IS_RELEASE = false

const configService = {
	apiUrl: IS_RELEASE ? prodEnv :devEnv
};

export default configService
