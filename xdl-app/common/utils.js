var common = {};
common.getCache = function(key) {
	return new Promise((succ, error) => {
		uni.getStorage({
			key: key,
			success: res => {
				succ(res.data)
			},
			fail: res => {
				console.log(res);
				error(res)
			}
		})
	})
}

common.getCacheSync = function(key) {
	try {
		const value = uni.getStorageSync(key);
		if (value) {
			return value
		}
	} catch (e) {
		return e
	}
}

common.removeCache=function(key){
	uni.removeStorageSync(key); 
}

common.setCache = function(key, data) {
	return new Promise((succ, error) => {
		uni.setStorage({
			key: key,
			data: data,
			success: res => {
				succ(res.data)
			},
			fail: res => {
				console.log(res);
				error(res)
			}
		})
	})
}




export default common
