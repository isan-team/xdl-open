# 新动力会员系统
当前最新版本： 1.0（发布日期：20220605）

[![AUR](https://img.shields.io/badge/license-LGLP3.0-blue.svg)](https://gitee.com/guocjsh/xdl-system/blob/master/LICENSE)
[![](https://img.shields.io/badge/version-1.0-brightgreen.svg)](https://gitee.com/guocjsh/xdl-system) 
[![](https://img.shields.io/badge/Author-俐羧科技-orange.svg)](http://www.isanlife.com)
#### 介绍
XDL-SYSTEM是由一个商业级项目升级优化而来的SpringBoot架构，采用Java8 API重构了业务代码，完全遵循阿里巴巴编码规范。采用Spring Boot 2 、Mybatis-plus、Jwt、wxjava，同时提供基于Vue的前端框架用于快速搭建企业级的系统平台。
XDL-SYSTEM致力于创造新颖的开发模式，将开发中遇到的痛点、生产中所踩的坑整理归纳，并将解决方案都融合到框架中。为XDL日常运营，赛事，会员权益，提供更快，更便捷的体验。实现新动力 eXciting Dream Lifestyle 的宗旨。

#### 软件架构
前后端分离架构

后端技术框架： SpringBoot + JWT+ MybatisPlus + Mysql + Redis

前端技术框架： vue

> 项目结构

```lua

 新动力系统
├── doc -- 文档
├── xdl-admin -- 运营后台管理系统
├── xdl-app -- 小程序端
├── xdl-service -- 新动力会员服务api接口
└── xdl-website -- 新动力官网
```

#### 架构图

##### 技术架构图

![系统架构图](https://gitee.com/guocjsh/xdl-system/raw/master/doc/%E6%96%B0%E5%8A%A8%E5%8A%9B%E5%B0%8F%E7%A8%8B%E5%BA%8F%E6%8A%80%E6%9C%AF%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88.png "Beluga微服务架构.png")

##### 使用手册
![使用手册](https://gitee.com/guocjsh/xdl-system/raw/master/doc/%E6%96%B0%E5%8A%A8%E5%8A%9B%E5%B0%8F%E7%A8%8B%E5%BA%8F%E6%BC%94%E7%A4%BA.png "系统架构图.png")

#### 合同共赢
![合同共赢](https://gitee.com/guocjsh/xdl-system/raw/master/doc/business.jpg "合同共赢.png")


#### 愿景
![愿景](https://gitee.com/guocjsh/xdl-system/raw/master/doc/slogen.jpg "愿景.png")