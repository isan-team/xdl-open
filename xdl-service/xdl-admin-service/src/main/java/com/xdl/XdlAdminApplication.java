package com.xdl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author iron guo
 * @since 2021.7.14
 * @des 新动力运营中台
 */
@MapperScan("com.xdl.modules.*.mapper")
@SpringBootApplication
public class XdlAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(XdlAdminApplication.class, args);
    }
}
