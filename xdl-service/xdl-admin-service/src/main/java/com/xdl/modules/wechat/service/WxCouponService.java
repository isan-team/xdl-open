package com.xdl.modules.wechat.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xdl.modules.wechat.entity.WxCoupon;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.wechat.model.CouponModel;
import com.xdl.modules.wechat.vo.WxCouponVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-05
 */
public interface WxCouponService extends IService<WxCoupon> {

    IPage<WxCouponVO> queryPage(CouponModel model);
}
