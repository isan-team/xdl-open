package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysUserDepart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysUserDepartMapper extends BaseMapper<SysUserDepart> {

}
