package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.WxInsurance;
import com.xdl.modules.wechat.mapper.WxInsuranceMapper;
import com.xdl.modules.wechat.service.WxInsuranceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
@Service
public class WxInsuranceServiceImpl extends ServiceImpl<WxInsuranceMapper, WxInsurance> implements WxInsuranceService {

}
