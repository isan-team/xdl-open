package com.xdl.modules.wechat.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.wechat.entity.WxOrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xdl.modules.wechat.model.OrderModel;
import com.xdl.modules.wechat.vo.OrderDetailVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
public interface WxOrderDetailMapper extends BaseMapper<WxOrderDetail> {

    /**
     * 订单列表
     * @param page
     * @param model
     * @return
     */
    IPage<OrderDetailVO> getOrderList(Page page,@Param("model") OrderModel model);
}
