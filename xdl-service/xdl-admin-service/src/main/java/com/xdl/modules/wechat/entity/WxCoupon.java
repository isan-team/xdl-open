package com.xdl.modules.wechat.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.common.utils.SnowflakeIdUtil;
import com.xdl.configuration.BaseContextHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-04-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxCoupon extends Model<WxCoupon> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id= SnowflakeIdUtil.nextId();

    /**
     * 优惠券标题
     */
    private String title;

    /**
     * 优惠券金额
     */
    private Integer money;

    /**
     * 使用商铺
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long shopId;

    /**
     * 使用规则
     */
    private String rule;

    /**
     * 有效期（day）
     */
    private Integer validity;

    /**
     * 使用方式
     */
    private String ruleRemark;

    /**
     * 开始时间
     */
    private LocalDateTime startDay;

    /**
     * 截止时间
     */
    private LocalDateTime endDay;

    /**
     * 创建时间
     */
    private LocalDateTime createTime=LocalDateTime.now();

    /**
     * 更新时间
     */
    private LocalDateTime updateTime=LocalDateTime.now();

    /**
     * 创建人
     */
    private String createBy= BaseContextHandler.getUsername();

    /**
     * 更新人
     */
    private String updateBy= BaseContextHandler.getUsername();

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel=0;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
