package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.WxBusiness;
import com.xdl.modules.wechat.mapper.WxBusinessMapper;
import com.xdl.modules.wechat.service.WxBusinessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
@Service
public class WxBusinessServiceImpl extends ServiceImpl<WxBusinessMapper, WxBusiness> implements WxBusinessService {

}
