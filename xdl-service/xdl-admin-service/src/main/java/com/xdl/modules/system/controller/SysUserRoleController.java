package com.xdl.modules.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户角色表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
@Controller
@RequestMapping("/sys-user-role")
public class SysUserRoleController {

}
