package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysDepartRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门角色权限表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysDepartRolePermissionMapper extends BaseMapper<SysDepartRolePermission> {

}
