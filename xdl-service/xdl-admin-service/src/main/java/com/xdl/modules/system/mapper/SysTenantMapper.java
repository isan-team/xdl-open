package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysTenant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 多租户信息表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysTenantMapper extends BaseMapper<SysTenant> {

}
