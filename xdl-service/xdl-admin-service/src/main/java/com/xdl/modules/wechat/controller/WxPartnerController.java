package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.wechat.entity.WxPartner;
import com.xdl.modules.wechat.model.PartnerModel;
import com.xdl.modules.wechat.service.WxPartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
@RestController
@RequestMapping("/wx/partner")
public class WxPartnerController {

    @Autowired
    private WxPartnerService partnerService;

    @PostMapping("/list")
    public ResponseResult<?> list(@RequestBody PartnerModel model){
        QueryWrapper<WxPartner> wrapper=new QueryWrapper<>();
        wrapper.lambda().like(StringUtils.isNotEmpty(model.getPartnerName()),WxPartner::getPartnerName,model.getPartnerName())
                .orderByAsc(WxPartner::getCreateTime);
        IPage<WxPartner> page = this.partnerService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        return ResponseResult.Success(page);
    }

    @PostMapping("save")
    public ResponseResult<Boolean> save(@RequestBody WxPartner partner){
        return ResponseResult.Success(this.partnerService.save(partner));
    }

    @PostMapping("edit")
    public ResponseResult<Boolean> edit(@RequestBody WxPartner partner){
        partner.setUpdateTime(LocalDateTime.now());
        partner.setUpdateBy(BaseContextHandler.getUsername());
        return ResponseResult.Success(this.partnerService.updateById(partner));
    }

    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        boolean remove = this.partnerService.removeById(id);
        return ResponseResult.Success(remove);
    }
}
