package com.xdl.modules.system.service.impl;

import com.xdl.modules.system.entity.SysUserRole;
import com.xdl.modules.system.mapper.SysUserRoleMapper;
import com.xdl.modules.system.service.SysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Override
    public List<String> getRoleByUserName(String username) {
        return this.baseMapper.getRoleByUserName(username);
    }
}
