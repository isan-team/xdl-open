package com.xdl.modules.wechat.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.wechat.entity.WxOrderDetail;
import com.xdl.modules.wechat.mapper.WxOrderDetailMapper;
import com.xdl.modules.wechat.model.OrderModel;
import com.xdl.modules.wechat.service.WxOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xdl.modules.wechat.vo.OrderDetailVO;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
@Service
public class WxOrderDetailServiceImpl extends ServiceImpl<WxOrderDetailMapper, WxOrderDetail> implements WxOrderDetailService {

    @Override
    public IPage<OrderDetailVO> getOrderList(OrderModel model) {
        return this.baseMapper.getOrderList(new Page<>(model.getPageNo(), model.getPageSize()),model);
    }
}
