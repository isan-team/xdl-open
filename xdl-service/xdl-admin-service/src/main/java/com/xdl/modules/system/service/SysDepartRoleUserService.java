package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysDepartRoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 部门角色用户表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysDepartRoleUserService extends IService<SysDepartRoleUser> {

    void deptRoleUserAdd(String userId, String newRoleId, String oldRoleId);

    void removeDeptRoleUser(List<String> asList, String depId);
}
