package com.xdl.modules.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.modules.system.entity.SysDictItem;
import com.xdl.modules.system.model.SysDictItemModel;
import com.xdl.modules.system.service.SysDictItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
@Slf4j
@RestController
@RequestMapping("/sys/dictItem")
public class SysDictItemController {

    @Autowired
    private SysDictItemService sysDictItemService;

    /**
     * @return
     * @功能：查询字典数据
     */
    @PostMapping("/list")
    public ResponseResult<IPage<SysDictItem>> queryPageList(@RequestBody SysDictItemModel model) {
        QueryWrapper<SysDictItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(StringUtils.isNotEmpty(model.getDictId()), SysDictItem::getDictId, model.getDictId())
                .eq(StringUtils.isNotEmpty(model.getStatus()), SysDictItem::getStatus, model.getStatus())
                .like(StringUtils.isNotEmpty(model.getItemText()), SysDictItem::getItemText, model.getItemText())
                .orderByAsc(SysDictItem::getSortOrder);
        Page<SysDictItem> page = new Page<SysDictItem>(model.getPageNo(), model.getPageSize());
        IPage<SysDictItem> pageList = sysDictItemService.page(page, queryWrapper);
        return ResponseResult.Success(pageList);
    }

    /**
     * @return
     * @功能：新增
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseResult<Boolean> add(@RequestBody SysDictItem sysDictItem) {
        sysDictItem.setCreateTime(LocalDateTime.now());
        boolean save = sysDictItemService.save(sysDictItem);
        return ResponseResult.Success(save);
    }

    /**
     * @param sysDictItem
     * @return
     * @功能：编辑
     */
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public ResponseResult<Boolean> edit(@RequestBody SysDictItem sysDictItem) {
        SysDictItem sysdict = sysDictItemService.getById(sysDictItem.getId());
        Assert.notNull(sysdict, "未找到对应实体");
        sysDictItem.setUpdateTime(LocalDateTime.now());
        boolean ok = sysDictItemService.updateById(sysDictItem);
        return ResponseResult.Success(ok);
    }

    /**
     * @param id
     * @return
     * @功能：删除字典数据
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseResult<Boolean> delete(@RequestParam(name = "id", required = true) String id) {
        SysDictItem joinSystem = sysDictItemService.getById(id);
        Assert.notNull(joinSystem, "未找到对应实体");
        boolean ok = sysDictItemService.removeById(id);
        return ResponseResult.Success(ok);
    }

    /**
     * @param ids
     * @return
     * @功能：批量删除字典数据
     */
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public ResponseResult<SysDictItem> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        Assert.notNull(ids, "参数不识别！");
        this.sysDictItemService.removeByIds(Arrays.asList(ids.split(",")));
        return ResponseResult.Success();
    }

    /**
     * 字典值重复校验
     *
     * @param sysDictItem
     * @param
     * @return
     */
    @RequestMapping(value = "/dictItemCheck", method = RequestMethod.POST)
    public ResponseResult<Object> doDictItemCheck(@RequestBody SysDictItem sysDictItem) {
        int num = 0;
        LambdaQueryWrapper<SysDictItem> queryWrapper = new LambdaQueryWrapper<SysDictItem>();
        queryWrapper.eq(SysDictItem::getItemValue, sysDictItem.getItemValue());
        queryWrapper.eq(SysDictItem::getDictId, sysDictItem.getDictId());
        if (StringUtils.isNotBlank(sysDictItem.getId())) {
            // 编辑页面校验
            queryWrapper.ne(SysDictItem::getId, sysDictItem.getId());
        }
        num = sysDictItemService.count(queryWrapper);
        if (num == 0) {
            // 该值可用
            return ResponseResult.Success("该值可用！");
        } else {
            // 该值不可用
            log.info("该值不可用，系统中已存在！");
            return ResponseResult.Error("该值不可用，系统中已存在！");
        }
    }

}
