package com.xdl.modules.system.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.response.ResponseCollection;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.modules.system.entity.SysDepartRole;
import com.xdl.modules.system.entity.SysDepartRoleUser;
import com.xdl.modules.system.model.SysDepartRoleModel;
import com.xdl.modules.system.service.SysDepartRoleService;
import com.xdl.modules.system.service.SysDepartRoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 部门角色表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
@RestController
@RequestMapping("/sys/sysDepartRole")
public class SysDepartRoleController {

    @Autowired
    private SysDepartRoleService sysDepartRoleService;

    @Autowired
    private SysDepartRoleUserService departRoleUserService;

    @PostMapping(value = "/list")
    public ResponseResult<IPage<SysDepartRole>> queryPageList(@RequestBody SysDepartRoleModel model) {
        QueryWrapper<SysDepartRole> queryWrapper = new QueryWrapper<>();
        Page<SysDepartRole> page = new Page<SysDepartRole>(model.getPageNo(), model.getPageSize());
        //我的部门，选中部门只能看当前部门下的角色
        queryWrapper.lambda()
                .eq(SysDepartRole::getDepartId,model.getDeptId())
                .eq(StringUtils.isNotEmpty(model.getRoleName()),SysDepartRole::getRoleName,model.getRoleName());
        IPage<SysDepartRole> pageList = sysDepartRoleService.page(page, queryWrapper);
        return ResponseResult.Success(pageList);
    }


    /**
     * 添加
     *
     * @param sysDepartRole
     * @return
     */
    @PostMapping(value = "/add")
    public ResponseResult<?> add(@RequestBody SysDepartRole sysDepartRole) {
        sysDepartRoleService.save(sysDepartRole);
        return ResponseResult.Success();
    }

    /**
     * 编辑
     *
     * @param sysDepartRole
     * @return
     */
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
    public ResponseResult<?> edit(@RequestBody SysDepartRole sysDepartRole) {
        sysDepartRoleService.updateById(sysDepartRole);
        return ResponseResult.Success();
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete")
    public ResponseResult<?> delete(@RequestParam(name="id",required=true) String id) {
        sysDepartRoleService.removeById(id);
        return ResponseResult.Success();
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatch")
    public ResponseResult<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.sysDepartRoleService.removeByIds(Arrays.asList(ids.split(",")));
        return ResponseResult.Success();
    }


    /**
     * 获取部门下角色
     * @param departId
     * @return
     */
    @GetMapping("/getDeptRoleList")
    public ResponseCollection<SysDepartRole> getDeptRoleList(@RequestParam(value = "departId") String departId){
        //查询选中部门的角色
        List<SysDepartRole> deptRoleList = sysDepartRoleService.list(new LambdaQueryWrapper<SysDepartRole>().eq(SysDepartRole::getDepartId,departId));
        return ResponseCollection.Success(deptRoleList);
    }

    /**
     * 根据用户id获取已设置部门角色
     * @param userId
     * @return
     */
    @GetMapping("/getDeptRoleByUserId")
    public ResponseCollection<SysDepartRoleUser> getDeptRoleByUserId(@RequestParam(value = "userId") String userId, @RequestParam(value = "departId") String departId){
        //查询部门下角色
        List<SysDepartRole> roleList = sysDepartRoleService.list(new QueryWrapper<SysDepartRole>().lambda().eq(SysDepartRole::getDepartId,departId));
        List<String> roleIds = roleList.stream().map(SysDepartRole::getId).collect(Collectors.toList());
        //根据角色id,用户id查询已授权角色
        List<SysDepartRoleUser> roleUserList = null;
        if(roleIds!=null && roleIds.size()>0){
            roleUserList = departRoleUserService.list(new QueryWrapper<SysDepartRoleUser>().lambda().eq(SysDepartRoleUser::getUserId,userId).in(SysDepartRoleUser::getDroleId,roleIds));
        }
        return ResponseCollection.Success(roleUserList);
    }


    /**
     * 设置
     * @param json
     * @return
     */
    @PostMapping("/deptRoleUserAdd")
    public ResponseResult<?> deptRoleAdd(@RequestBody JSONObject json) {
        String newRoleId = json.getString("newRoleId");
        String oldRoleId = json.getString("oldRoleId");
        String userId = json.getString("userId");
        departRoleUserService.deptRoleUserAdd(userId,newRoleId,oldRoleId);
        return ResponseResult.Success();
    }



}
