package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.util.Func;
import com.xdl.common.response.ResponseResult;
import com.xdl.modules.wechat.entity.WxUser;
import com.xdl.modules.wechat.model.WxUserModel;
import com.xdl.modules.wechat.service.WxUserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * <p>
 * 微信用户表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-05-16
 */
@AllArgsConstructor
@BelugaController("/wx/user")
public class WxUserController {

    private WxUserService userService;

    /**
     * 微信用户列表
     * @param model
     * @return
     */
    @PostMapping("list")
    public ResponseResult<IPage<WxUser>> list(@RequestBody WxUserModel model){
        QueryWrapper<WxUser> wrapper=new QueryWrapper<>();
        wrapper.lambda().like(Func.isNotEmpty(model.getNickName()),WxUser::getNickname,model.getNickName())
                        .like(Func.isNotEmpty(model.getPhone()),WxUser::getPhone,model.getPhone())
                        .orderByDesc(WxUser::getCreateTime);
        IPage<WxUser> page = userService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        return ResponseResult.Success(page);
    }


    /**
     * 更改VIP等级
     */
    @PostMapping("setVip")
    public ResponseResult<Boolean> setVip(@RequestBody WxUser user){
        WxUser wxUser = userService.getById(user);
        wxUser.setLevel(user.getLevel());
        return ResponseResult.Success(userService.updateById(wxUser));
    }




}
