package com.xdl.modules.wechat.service;

import com.xdl.modules.wechat.entity.WxInsurance;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
public interface WxInsuranceService extends IService<WxInsurance> {

}
