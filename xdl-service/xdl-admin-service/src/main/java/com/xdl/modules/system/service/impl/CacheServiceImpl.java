package com.xdl.modules.system.service.impl;

import com.xdl.common.utils.RedisUtils;
import com.xdl.modules.system.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CacheServiceImpl implements CacheService {

    @Autowired
    private RedisUtils cache;


    @Override
    public boolean cacheRandomImage(String realKey, String lowerCaseCode, int time) {
        return cache.set(realKey, lowerCaseCode, time);
    }

    @Override
    public Object getRandomImage(String realKey) {
        return cache.get(realKey);
    }

    @Override
    public Object getCache(String key) {
        return cache.get(key);
    }


    @Override
    public void delKey(String realKey) {
       cache.del(realKey);
    }

    @Override
    public boolean setCache(String key, String value, long time) {
        return cache.set(key, value, time);
    }
}
