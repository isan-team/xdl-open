package com.xdl.modules.system.controller;


import com.xdl.common.constants.CommonConstants;
import com.xdl.common.response.ResponseCollection;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.system.entity.SysDepart;
import com.xdl.modules.system.entity.SysUser;
import com.xdl.modules.system.model.DepartIdModel;
import com.xdl.modules.system.model.SysDepartTreeModel;
import com.xdl.modules.system.service.SysDepartService;
import com.xdl.modules.system.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 组织机构表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
@RestController
@RequestMapping("/sys/sysDepart")
@Slf4j
public class SysDepartController {

    @Autowired
    private SysDepartService sysDepartService;

    @Autowired
    private SysUserService sysUserService;

    @GetMapping("queryTreeList")
    public ResponseCollection<SysDepartTreeModel> queryTreeList(@RequestParam(name = "ids", required = false) String ids) {
        List<SysDepartTreeModel> departList = sysDepartService.queryTreeList(ids);
        return ResponseCollection.Success(departList);
    }

    /**
     * 查询数据 添加或编辑页面对该方法发起请求,以树结构形式加载所有部门的名称,方便用户的操作
     *
     * @return
     */
    @GetMapping("/queryIdTree")
    public ResponseCollection<DepartIdModel> queryIdTree() {
        List<DepartIdModel> list = sysDepartService.queryDepartIdTreeList();
        return ResponseCollection.Success(list);
    }

    /**
     * 添加新数据 添加用户新建的部门对象数据,并保存到数据库
     *
     * @param sysDepart
     * @return
     */
    @PostMapping("/add")
    public ResponseResult<Boolean> add(@RequestBody SysDepart sysDepart) {
        String username = BaseContextHandler.getUsername();
        sysDepart.setCreateBy(username);
        boolean save = sysDepartService.saveDepartData(sysDepart, username);
        return ResponseResult.Success(save);
    }

    /**
     * 编辑数据 编辑部门的部分数据,并保存到数据库
     *
     * @param sysDepart
     * @return
     */
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
    public ResponseResult<Boolean> edit(@RequestBody SysDepart sysDepart, HttpServletRequest request) {
        String username =  BaseContextHandler.getUsername();
        sysDepart.setUpdateBy(username);
        SysDepart sysDepartEntity = sysDepartService.getById(sysDepart.getId());
        if (sysDepartEntity == null) {
            return ResponseResult.Error("对象不存在！");
        } else {
            boolean update = sysDepartService.updateDepartDataById(sysDepart, username);
            return ResponseResult.Success(update);
        }
    }


    /**
     * 批量删除 根据前端请求的多个ID,对数据库执行删除相关部门数据的操作
     *
     * @param ids
     * @return
     */
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public ResponseResult<SysDepart> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        Assert.notNull(ids, "参数不识别！");
        this.sysDepartService.deleteBatchWithChildren(Arrays.asList(ids.split(",")));
        return ResponseResult.Success();
    }


    /**
     * 查询数据 查出我的部门,并以树结构数据格式响应给前端
     *
     * @return
     */
    @GetMapping("/queryMyDeptTreeList")
    public ResponseCollection<SysDepartTreeModel> queryMyDeptTreeList() {
        String userid = BaseContextHandler.getUserID();
        Assert.notNull(userid,"账号登陆异常！");
        SysUser sysUser = sysUserService.getById(userid);
        if(StringUtils.isNotEmpty(sysUser.getUserIdentity()) && sysUser.getUserIdentity().equals( CommonConstants.USER_IDENTITY_2 )) {
            String departIds = sysUser.getDepartIds();
            if (StringUtils.isNotBlank(departIds)) {
                List<SysDepartTreeModel> list = sysDepartService.queryMyDeptTreeList(departIds);
                return ResponseCollection.Success(list);
            }
        }
        return ResponseCollection.Success();
    }


    /**
     * <p>
     * 部门搜索功能方法,根据关键字模糊搜索相关部门
     * </p>
     *
     * @param keyWord
     * @return
     */
    @RequestMapping(value = "/searchBy", method = RequestMethod.GET)
    public ResponseCollection<SysDepartTreeModel> searchBy(@RequestParam(name = "keyWord", required = true) String keyWord,@RequestParam(name = "myDeptSearch", required = false) String myDeptSearch) {
        //部门查询，myDeptSearch为1时为我的部门查询，登录用户为上级时查只查负责部门下数据
        String userid = BaseContextHandler.getUserID();
        Assert.notNull(userid, "账号登陆异常！");
        SysUser user = sysUserService.getById(userid);
        String departIds = null;
        if(StringUtils.isNotEmpty(user.getUserIdentity()) && user.getUserIdentity().equals( CommonConstants.USER_IDENTITY_2 )){
            departIds = user.getDepartIds();
        }
        List<SysDepartTreeModel> treeList = this.sysDepartService.searchByKeyWord(keyWord,myDeptSearch,departIds);
        return ResponseCollection.Success(treeList);
    }



}
