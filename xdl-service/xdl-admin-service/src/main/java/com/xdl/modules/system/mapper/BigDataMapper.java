package com.xdl.modules.system.mapper;

import com.xdl.modules.system.model.BigDataModel;
import com.xdl.modules.system.model.MonthSalesModel;
import com.xdl.modules.system.model.StoreModel;

import java.util.List;

public interface BigDataMapper {

    /**
     * 获取大数据销售指标
     * @return
     */
    BigDataModel getBigDataSource();

    /**
     * 获取12个月销售额
     * @return
     */
    List<MonthSalesModel> monthSales();

    /**
     * 获取门店销售业绩
     * @return
     */
    List<StoreModel> getStoreData();
}
