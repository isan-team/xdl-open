package com.xdl.modules.wechat.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.wechat.entity.WxActivityDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xdl.modules.wechat.vo.WxActivityDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动详情 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
public interface WxActivityDetailMapper extends BaseMapper<WxActivityDetail> {

    IPage<WxActivityDetailVO> queryPage(Page page, @Param("title") String title,@Param("status") Integer status);

    /**
     * 获取过期活动
     * @return
     */
    List<WxActivityDetail> getExpiredActivity();
}
