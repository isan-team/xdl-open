package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.util.Func;
import com.xdl.common.response.ResponseCollection;
import com.xdl.common.response.ResponseResult;
import com.xdl.modules.wechat.entity.WxActivityDetail;
import com.xdl.modules.wechat.entity.WxInsurance;
import com.xdl.modules.wechat.model.InsuranceModel;
import com.xdl.modules.wechat.service.WxActivityDetailService;
import com.xdl.modules.wechat.service.WxInsuranceService;
import lombok.AllArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
@AllArgsConstructor
@BelugaController("/wx/insurance")
public class WxInsuranceController {

    private WxInsuranceService insuranceService;

    private WxActivityDetailService activityService;

    /**
     * 获取新动力签约保险列表
     * @param model
     * @return
     */
    @PostMapping("list")
    public ResponseResult<IPage<WxInsurance>> list(@RequestBody InsuranceModel model){
        QueryWrapper<WxInsurance> wrapper=new QueryWrapper<>();
        wrapper.lambda()
                .like(Func.isNotEmpty(model.getInsurance()),WxInsurance::getInsuranceName,model.getInsurance())
                .orderByDesc(WxInsurance::getCreateTime);
        IPage<WxInsurance> result = insuranceService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        return ResponseResult.Success(result);
    }

    /**
     * 获取所有保险
     * @return
     */
    @GetMapping("getAll")
    public ResponseCollection<WxInsurance> getAll(){
        List<WxInsurance> list = insuranceService.list();
        return ResponseCollection.Success(list);
    }

    /**
     * 编辑保险内容
     * @param insurance
     * @return
     */
    @PostMapping("edit")
    public ResponseResult<?> edit(@RequestBody WxInsurance insurance){
        WxInsurance wxInsurance = insuranceService.getById(insurance);
        Func.copy(insurance,wxInsurance);
        Assert.notNull(wxInsurance,"系统未找到该类保险！");
        return ResponseResult.Success(insuranceService.updateById(wxInsurance));
    }

    /**
     * 编辑保险内容
     * @param insurance
     * @return
     */
    @PostMapping("save")
    public ResponseResult<Boolean> save(@RequestBody WxInsurance insurance){
        boolean save = insuranceService.save(insurance);
        return ResponseResult.Success(save);
    }


    /**
     * 删除保险
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        QueryWrapper<WxActivityDetail> actwrapper=new QueryWrapper<>();
        actwrapper.lambda().eq(WxActivityDetail::getInsurance,id);
        int count = activityService.count(actwrapper);
        if(count>0){
            return ResponseResult.Error("该保险已被引用！");
        }else {
            boolean remove = insuranceService.removeById(id);
            return ResponseResult.Success(remove);
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public ResponseResult<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        Assert.notNull(ids, "未选中保险！");
        String[] ls = ids.split(",");
        // 过滤掉已被引用的租户
        List<Long> idList = new ArrayList<>();
        for (String id : ls) {
            QueryWrapper<WxActivityDetail> actwrapper=new QueryWrapper<>();
            actwrapper.lambda().eq(WxActivityDetail::getInsurance,id);
            int count = activityService.count(actwrapper);
            if(count==0){
                idList.add(Long.valueOf(id));
            }
        }
        if (idList.size() > 0) {
            insuranceService.removeByIds(idList);
            return ResponseResult.Success("删除成功！");
        } else {
            return ResponseResult.Error("选择的保险都已被引用，无法删除！");
        }
    }



}
