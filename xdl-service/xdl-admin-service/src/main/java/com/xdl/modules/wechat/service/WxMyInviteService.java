package com.xdl.modules.wechat.service;

import com.xdl.modules.wechat.entity.WxMyInvite;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-24
 */
public interface WxMyInviteService extends IService<WxMyInvite> {

    /**
     * 获取可激活优惠券
     * @return
     */
    List<WxMyInvite> effectList();
}
