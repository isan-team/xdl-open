/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.system.controller;

import com.beluga.core.annotation.BelugaController;
import com.xdl.common.response.ResponseCollection;
import com.xdl.common.response.ResponseResult;
import com.xdl.modules.system.model.BigDataModel;
import com.xdl.modules.system.model.MonthSalesModel;
import com.xdl.modules.system.model.StoreModel;
import com.xdl.modules.system.service.BigDataService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @Author iron.guo
 * @Date 2022/5/20
 * @Description
 */
@AllArgsConstructor
@BelugaController("/sys/bigdata")
public class BigDataController {

    private BigDataService bigDataService;

    /**
     * 获取销售指标
     * @return
     */
    @GetMapping("getBigDataSource")
    public ResponseResult<BigDataModel> getBigDataSource(){
        BigDataModel result=bigDataService.getBigDataSource();
        return ResponseResult.Success(result);
    }

    /**
     * 获取12个月的每个月销售额
     * @return
     */
    @GetMapping("monthSales")
    public ResponseCollection<MonthSalesModel> monthSales(){
        List<MonthSalesModel> result=bigDataService.monthSales();
        return ResponseCollection.Success(result);
    }

    /**
     * 获取门店销售额
     * @return
     */
    @GetMapping("getStoreData")
    public ResponseCollection<StoreModel> getStoreData(){
        List<StoreModel> result=bigDataService.getStoreData();
        return ResponseCollection.Success(result);
    }


}
