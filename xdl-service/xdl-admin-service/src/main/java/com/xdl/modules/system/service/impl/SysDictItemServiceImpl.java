package com.xdl.modules.system.service.impl;

import com.xdl.modules.system.entity.SysDictItem;
import com.xdl.modules.system.mapper.SysDictItemMapper;
import com.xdl.modules.system.service.SysDictItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements SysDictItemService {

}
