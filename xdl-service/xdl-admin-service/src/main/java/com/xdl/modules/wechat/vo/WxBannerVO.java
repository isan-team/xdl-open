package com.xdl.modules.wechat.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxBannerVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * banner名称
     */
    private String title;

    /**
     * banner图地址
     */
    private String bannerUrl;

    /**
     * 活动id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long actId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    private String createBy;

    private String actitle;

}
