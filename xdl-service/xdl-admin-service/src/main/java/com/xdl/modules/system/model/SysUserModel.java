package com.xdl.modules.system.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserModel extends BasePage{

    private String departId;

    private String status;

    private String code;

    private String phone;

    private String realname;

    private String sex;

    private String username;

}
