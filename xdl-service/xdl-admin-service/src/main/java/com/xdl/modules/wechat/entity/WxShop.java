package com.xdl.modules.wechat.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.wechat.model.ShopModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商户表
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxShop extends Model<WxShop> {

    private static final long serialVersionUID = 1L;

    /**
     * 商户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 商户名称
     */
    private String shopName;

    /**
     * 商户icon
     */
    private String shopIcon;

    /**
     * 商户所在城市
     */
    private String city;

    /**
     * 商户地址
     */
    private String adress;

    /**
     * 经纬度
     */
    private String location;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel=0;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


    public WxShop(){}

    public WxShop(ShopModel model){
        this.shopName=model.getBaseName();
        this.city=model.getLocation().getPname();
        this.adress=model.getLocation().getAdress();
        this.location=model.getLocation().getLocation();
        this.createTime=LocalDateTime.now();
        this.updateTime=LocalDateTime.now();
        this.createBy= BaseContextHandler.getUsername();
        this.updateBy=BaseContextHandler.getUsername();
    }



}
