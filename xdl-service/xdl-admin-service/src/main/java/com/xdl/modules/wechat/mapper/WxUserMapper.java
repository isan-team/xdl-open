package com.xdl.modules.wechat.mapper;

import com.xdl.modules.wechat.entity.WxUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 微信用户表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-16
 */
public interface WxUserMapper extends BaseMapper<WxUser> {

}
