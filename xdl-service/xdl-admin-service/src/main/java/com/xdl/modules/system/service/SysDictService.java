package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.system.model.DictModel;
import com.xdl.modules.system.model.DuplicateCheckModel;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysDictService extends IService<SysDict> {

    Map<String, List<DictModel>> queryAllDictItems();

    Long duplicateCheckCountSql(DuplicateCheckModel model);

    Long duplicateCheckCountSqlNoDataId(DuplicateCheckModel model);

    List<DictModel> getDictItems(String dictCode);

    List<SysDict> queryDeleteList();

    void deleteOneDictPhysically(String id);

    boolean updateDictDelFlag(int i, String id);
}
