package com.xdl.modules.system.strategy;

import com.alibaba.fastjson.JSONObject;
import com.xdl.configuration.StringBeanUtils;
import com.xdl.modules.system.service.IFillRuleHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FillRuleStrategy {


    public Object executeRule(String ruleCode,JSONObject formData){
        IFillRuleHandler handler=this.buildMethod(ruleCode);
        return handler.execute(formData);
    }

    /**
     * 私有方法获取策略
     *
     * @param
     * @return
     */
    private IFillRuleHandler buildMethod(String ruleCode) {
        if (StringUtils.isEmpty(ruleCode)) {
            log.error("策略不能为空....");
            return null;
        }
        IFillRuleHandler service = StringBeanUtils.getBean(StrategyEnum.valueOf(ruleCode).getClassName(), IFillRuleHandler.class);
        if (service == null) {
            log.error("没有找到具体得策略实现....");
            return null;
        }
        return service;
    }



}
