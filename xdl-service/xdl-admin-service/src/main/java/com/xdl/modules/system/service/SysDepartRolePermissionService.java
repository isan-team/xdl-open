package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysDepartRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部门角色权限表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysDepartRolePermissionService extends IService<SysDepartRolePermission> {

    boolean saveDeptRolePermission(String roleId, String permissionIds, String lastPermissionIds);
}
