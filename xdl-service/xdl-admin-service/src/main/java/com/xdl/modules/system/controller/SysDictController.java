package com.xdl.modules.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.constants.CommonConstants;
import com.xdl.common.response.ResponseCollection;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.modules.system.entity.SysDict;
import com.xdl.modules.system.model.DictModel;
import com.xdl.modules.system.model.SysDictModel;
import com.xdl.modules.system.service.SysDictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
@Slf4j
@RestController
@RequestMapping("/sys/dict")
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;

    /**
     * 获取字典数据 【接口签名验证】
     *
     * @param dictCode 字典code
     * @param dictCode 表名,文本字段,code字段  | 举例：sys_user,realname,id
     * @return
     */
    @RequestMapping(value = "/getDictItems/{dictCode}", method = RequestMethod.GET)
    public ResponseCollection<DictModel> getDictItems(@PathVariable String dictCode, @RequestParam(value = "sign", required = false) String sign, HttpServletRequest request) {
        List<DictModel> ls = sysDictService.getDictItems(dictCode);
        if (ls == null) {
            return ResponseCollection.Error("字典Code格式不正确！");
        }
        return ResponseCollection.Success(ls);
    }


    @PostMapping("/list")
    public ResponseResult<IPage<SysDict>> queryPageList(@RequestBody SysDictModel model) {
        QueryWrapper<SysDict> queryWrapper = new QueryWrapper<>();
        Page<SysDict> page = new Page<SysDict>(model.getPageNo(), model.getPageSize());
        queryWrapper.lambda()
                .eq(StringUtils.isNotEmpty(model.getDictCode()), SysDict::getDictCode, model.getDictCode())
                .eq(StringUtils.isNotEmpty(model.getDictName()), SysDict::getDictName, model.getDictName())
                .orderByDesc(SysDict::getCreateTime);
        IPage<SysDict> pageList = sysDictService.page(page, queryWrapper);
        return ResponseResult.Success(pageList);
    }

    /**
     * @param sysDict
     * @return
     * @功能：新增
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseResult<Boolean> add(@RequestBody SysDict sysDict) {
        sysDict.setCreateTime(LocalDateTime.now());
        sysDict.setDelFlag(CommonConstants.DEL_FLAG_0);
        boolean save = sysDictService.save(sysDict);
        return ResponseResult.Success(save);
    }

    /**
     * @param sysDict
     * @return
     * @功能：编辑
     */
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public ResponseResult<Boolean> edit(@RequestBody SysDict sysDict) {
        SysDict sysdict = sysDictService.getById(sysDict.getId());
        Assert.notNull(sysdict, "未找到对应实体");
        sysDict.setUpdateTime(LocalDateTime.now());
        boolean ok = sysDictService.updateById(sysDict);
        return ResponseResult.Success(ok);
    }

    /**
     * @param id
     * @return
     * @功能：删除
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseResult<Boolean> delete(@RequestParam(name = "id", required = true) String id) {
        boolean ok = sysDictService.removeById(id);
        return ResponseResult.Success(ok);
    }

    /**
     * @param ids
     * @return
     * @功能：批量删除
     */
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public ResponseResult<Boolean> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        Assert.notNull(ids, "参数不识别！");
        boolean removeByIds = sysDictService.removeByIds(Arrays.asList(ids.split(",")));
        return ResponseResult.Success(removeByIds);
    }

    /**
     * 查询被删除的列表
     *
     * @return
     */
    @RequestMapping(value = "/deleteList", method = RequestMethod.GET)
    public ResponseCollection<SysDict> deleteList() {
        List<SysDict> list = this.sysDictService.queryDeleteList();
        return ResponseCollection.Success(list);
    }


    /**
     * 物理删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/deletePhysic/{id}", method = RequestMethod.DELETE)
    public ResponseResult<?> deletePhysic(@PathVariable String id) {
        sysDictService.deleteOneDictPhysically(id);
        return ResponseResult.Success();
    }

    /**
     * 逻辑删除的字段，进行取回
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/back/{id}", method = RequestMethod.PUT)
    public ResponseResult<?> back(@PathVariable String id) {
        boolean back=sysDictService.updateDictDelFlag(0, id);
        return ResponseResult.Success(back);
    }


}
