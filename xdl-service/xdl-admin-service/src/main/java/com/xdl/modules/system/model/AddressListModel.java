package com.xdl.modules.system.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AddressListModel extends BasePage{

   private String orgCode;

   private String realname;

   private String workNo;



}
