/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.task;

import com.beluga.core.util.Func;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.wechat.entity.WxActivityDetail;
import com.xdl.modules.wechat.entity.WxCoupon;
import com.xdl.modules.wechat.entity.WxMyInvite;
import com.xdl.modules.wechat.entity.WxOwnerCoupon;
import com.xdl.modules.wechat.service.WxActivityDetailService;
import com.xdl.modules.wechat.service.WxCouponService;
import com.xdl.modules.wechat.service.WxMyInviteService;
import com.xdl.modules.wechat.service.WxOwnerCouponService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * @Author iron.guo
 * @Date 2022/5/24
 * @Description 定时任务管理
 */
@Slf4j
@Component
@EnableScheduling
@AllArgsConstructor
public class TaskController {

    private WxActivityDetailService activityService;

    private WxMyInviteService inviteService;

    private WxOwnerCouponService ownerCouponService;

    private WxCouponService couponService;

    /**
     * 活动是否过期任务
     * n+1任务
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void activityTask(){
       List<WxActivityDetail> list= activityService.getExpiredActivity();
       if(list.size()>0){
           list.forEach(i->{
               i.setStatus(3);
               activityService.updateById(i);
               log.warn("{} 活动已过期...",i.getTitle());
           });
       }
    }

    /**
     * 是否邀请成功定时任务
     * n+1任务
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void inviteTask(){
        List<WxMyInvite> list=inviteService.effectList();
        if(list.size()>0){
            list.forEach(i->{
                i.setEffect(1);
                inviteService.updateById(i);
                putCoupon(2l,i.getUserId());
            });
        }
    }

    /**
     * 优惠券是否过期任务
     * n+1任务
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void ticketTask(){
        List<WxOwnerCoupon> list=ownerCouponService.couponExpired();
        if(list.size()>0){
            list.forEach(i->{
                i.setStatus(3);
                ownerCouponService.updateById(i);
            });
        }
    }

    /**
     * <p>
     *   发放优惠券
     * </p>
     * @param id
     * @param userId
     * @return
     */
    private Boolean putCoupon(Long id,Long userId) {
        WxCoupon wxCoupon = couponService.getById(id);
        WxOwnerCoupon owner = new WxOwnerCoupon();
        if (Func.isEmpty(wxCoupon.getValidity())) {
            owner.setStartDay(wxCoupon.getStartDay());
            owner.setEndDay(wxCoupon.getEndDay());
        } else {
            LocalDate localDate = LocalDate.now();
            owner.setStartDay(localDate.atStartOfDay());
            owner.setEndDay(localDate.plus(wxCoupon.getValidity(), ChronoUnit.DAYS).atStartOfDay());
        }
        owner.setCouponId(wxCoupon.getId());
        owner.setStatus(1);
        owner.setUserId(userId);
        boolean save = this.ownerCouponService.save(owner);
        return save;
    }



}
