package com.xdl.modules.wechat.service;

import com.xdl.modules.wechat.entity.WxUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 微信用户表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-16
 */
public interface WxUserService extends IService<WxUser> {

}
