package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.WxOwnerCoupon;
import com.xdl.modules.wechat.mapper.WxOwnerCouponMapper;
import com.xdl.modules.wechat.service.WxOwnerCouponService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-24
 */
@Service
public class WxOwnerCouponServiceImpl extends ServiceImpl<WxOwnerCouponMapper, WxOwnerCoupon> implements WxOwnerCouponService {

    @Override
    public List<WxOwnerCoupon> couponExpired() {
        return this.baseMapper.couponExpired();
    }
}
