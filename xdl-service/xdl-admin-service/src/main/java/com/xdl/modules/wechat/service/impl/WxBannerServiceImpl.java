package com.xdl.modules.wechat.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.wechat.entity.WxBanner;
import com.xdl.modules.wechat.mapper.WxBannerMapper;
import com.xdl.modules.wechat.model.BannerModel;
import com.xdl.modules.wechat.service.WxBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xdl.modules.wechat.vo.WxBannerVO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * banner图表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@Service
public class WxBannerServiceImpl extends ServiceImpl<WxBannerMapper, WxBanner> implements WxBannerService {

    @Override
    public IPage<WxBannerVO> getPage(BannerModel model) {
        return this.baseMapper.getPage(new Page<>(model.getPageNo(), model.getPageSize()),model.getActName());
    }
}
