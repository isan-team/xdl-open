package com.xdl.modules.system.service;

import com.alibaba.fastjson.JSONObject;

/**
 * 填值规则接口
 *
 * @author iron.guo
 * 如需使用填值规则功能，规则实现类必须实现此接口
 */
public interface IFillRuleHandler {

    /**
     * @param formData  动态表单参数
     * @return
     */
    Object execute(JSONObject formData);

}

