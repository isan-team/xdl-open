package com.xdl.modules.wechat.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxActivityDetailVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 活动名称
     */
    private String title;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long insurance;

    /**
     * 是否需要提前支付
     */
    private Integer needPay;

    /**
     * 活动内容
     */
    private String actName;

    /**
     * 商户id
     */
    private String shopName;

    /**
     * 活动会员等级规则
     */
    private Integer menberLevel;


    /**
     * 商户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long shopId;

    /**
     * 价格
     */
    private Double price;

    /**
     * 城市
     */
    private String city;

    /**
     * 地址
     */
    private String address;


    /**
     * 活动开始时间
     */
    private Date startDay;

    /**
     * 活动结束时间
     */
    private Date endDay;

    /**
     * 可预约天数
     */
    private Integer deadline;

    /**
     * 图文
     */
    private String imageText;

    /**
     * 轮播图
     */
    private String images;

    /**
     * 活动主图
     */
    private String mainPic;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 活动类型
     */
    private String classify;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新人
     */
    private String updateBy;
}
