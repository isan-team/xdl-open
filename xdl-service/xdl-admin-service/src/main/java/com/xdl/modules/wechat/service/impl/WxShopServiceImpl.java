package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.WxShop;
import com.xdl.modules.wechat.mapper.WxShopMapper;
import com.xdl.modules.wechat.service.WxShopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商户表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@Service
public class WxShopServiceImpl extends ServiceImpl<WxShopMapper, WxShop> implements WxShopService {

}
