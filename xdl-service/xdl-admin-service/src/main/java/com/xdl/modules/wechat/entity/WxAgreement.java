package com.xdl.modules.wechat.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.common.utils.SnowflakeIdUtil;
import com.xdl.configuration.BaseContextHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxAgreement extends Model<WxAgreement> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.INPUT)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id= SnowflakeIdUtil.nextId();

    /**
     * 协议名称
     */
    private String agreementName;

    /**
     * 协议码
     */
    private String agreementCode;

    /**
     * 协议内容
     */
    private String agreementContent;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel=0;

    /**
     * 创建时间
     */
    private LocalDateTime createTime=LocalDateTime.now();

    /**
     * 更新时间
     */
    private LocalDateTime updateTime=LocalDateTime.now();

    /**
     * 创建人
     */
    private String createBy= BaseContextHandler.getUsername();

    /**
     * 更新人
     */
    private String updateBy=BaseContextHandler.getUsername();


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
