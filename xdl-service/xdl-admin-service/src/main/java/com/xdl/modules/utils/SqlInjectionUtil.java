package com.xdl.modules.utils;

import com.xdl.common.exception.BelugaException;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

/**
 * sql注入处理工具类
 */
@Slf4j
public class SqlInjectionUtil {

	private final static String xssStr = "and |exec |insert |select |delete |update |drop |count |chr |mid |master |truncate |char |declare |;|or |+|user()";


	/**
	 * sql注入过滤处理，遇到注入关键字抛异常
	 * 
	 * @param value
	 * @return
	 */
	public static void filterContent(String value) {
		if (value == null || "".equals(value)) {
			return;
		}
		// 统一转为小写
		value = value.toLowerCase();
		//SQL注入检测存在绕过风险 https://gitee.com/jeecg/jeecg-boot/issues/I4NZGE
		value = value.replaceAll("/\\*.*\\*/","");

		String[] xssArr = xssStr.split("\\|");
		for (int i = 0; i < xssArr.length; i++) {
			if (value.indexOf(xssArr[i]) > -1) {
				log.error("请注意，存在SQL注入关键词---> {}", xssArr[i]);
				log.error("请注意，值可能存在SQL注入风险!---> {}", value);
				BelugaException.fatal("请注意，值可能存在SQL注入风险!--->" + value);
			}
		}
		if(Pattern.matches("show\\s+tables", value)){
			BelugaException.fatal("请注意，值可能存在SQL注入风险!--->" + value);
		}
		return;
	}

	/**
	 * sql注入过滤处理，遇到注入关键字抛异常
	 *
	 * @param values
	 * @return
	 */
	public static void filterContent(String[] values) {
		String[] xssArr = xssStr.split("\\|");
		for (String value : values) {
			if (value == null || "".equals(value)) {
				return;
			}
			// 统一转为小写
			value = value.toLowerCase();
			value = value.replaceAll("/\\*.*\\*/","");

			for (int i = 0; i < xssArr.length; i++) {
				if (value.indexOf(xssArr[i]) > -1) {
					log.error("请注意，存在SQL注入关键词---> {}", xssArr[i]);
					log.error("请注意，值可能存在SQL注入风险!---> {}", value);
					BelugaException.fatal("请注意，值可能存在SQL注入风险!--->" + value);
				}
			}
			if(Pattern.matches("show\\s+tables", value)){
				BelugaException.fatal("请注意，值可能存在SQL注入风险!--->" + value);
			}
		}
		return;
	}


}
