package com.xdl.modules.wechat.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.common.utils.SnowflakeIdUtil;
import com.xdl.configuration.BaseContextHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * banner图表
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxBanner implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.INPUT)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id=SnowflakeIdUtil.nextId();

    /**
     * banner名称
     */
    private String title;

    /**
     * banner图地址
     */
    private String bannerUrl;

    /**
     * 活动id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long actId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime=LocalDateTime.now();

    /**
     * 更新时间
     */
    private LocalDateTime updateTime=LocalDateTime.now();

    /**
     * 创建人
     */
    private String createBy= BaseContextHandler.getUsername();

    /**
     * 更新人
     */
    private String updateBy= BaseContextHandler.getUsername();

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel=0;


}
