package com.xdl.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xdl.modules.system.entity.SysLog;
import com.xdl.modules.system.mapper.SysLogMapper;
import com.xdl.modules.system.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统日志表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

    @Override
    public Long findTotalVisitCount() {
        LambdaQueryWrapper<SysLog> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(SysLog::getLogType,1);
        Integer count = this.baseMapper.selectCount(wrapper);
        return Long.valueOf(count);
    }

    @Override
    public Long findTodayVisitCount(Date dayStart, Date dayEnd) {
        LambdaQueryWrapper<SysLog> wrapper=new LambdaQueryWrapper<>();
        wrapper.ge(SysLog::getCreateTime,dayStart).lt(SysLog::getCreateTime,dayEnd);
        Integer count = this.baseMapper.selectCount(wrapper);
        return Long.valueOf(count);
    }

    @Override
    public Long findTodayIp(Date dayStart, Date dayEnd) {
        return 2l;
    }

    @Override
    public List<Map<String, Object>> findVisitCount(Date dayStart, Date dayEnd) {
        return null;
    }
}
