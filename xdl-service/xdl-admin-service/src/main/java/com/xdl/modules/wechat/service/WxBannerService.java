package com.xdl.modules.wechat.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xdl.modules.wechat.entity.WxBanner;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.wechat.model.BannerModel;
import com.xdl.modules.wechat.vo.WxBannerVO;

/**
 * <p>
 * banner图表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
public interface WxBannerService extends IService<WxBanner> {

    IPage<WxBannerVO> getPage(BannerModel model);
}
