package com.xdl.modules.wechat.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.wechat.entity.WxActivityDetail;
import com.xdl.modules.wechat.mapper.WxActivityDetailMapper;
import com.xdl.modules.wechat.model.ActivityModel;
import com.xdl.modules.wechat.service.WxActivityDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xdl.modules.wechat.vo.WxActivityDetailVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 活动详情 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@Service
public class WxActivityDetailServiceImpl extends ServiceImpl<WxActivityDetailMapper, WxActivityDetail> implements WxActivityDetailService {

    @Override
    public IPage<WxActivityDetailVO> queryPage(ActivityModel model) {
        return this.baseMapper.queryPage(new Page<>(model.getPageNo(), model.getPageSize()),model.getTitle(),model.getStatus());
    }

    @Override
    public List<WxActivityDetail> getExpiredActivity() {
        return this.baseMapper.getExpiredActivity();
    }
}
