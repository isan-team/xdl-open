package com.xdl.modules.wechat.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.common.utils.SnowflakeIdUtil;
import com.xdl.configuration.BaseContextHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * Oss File
 * </p>
 *
 * @author iron guo
 * @since 2022-05-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysOssFile extends Model<SysOssFile> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id= SnowflakeIdUtil.nextId();

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件地址
     */
    private String url;

    /**
     * 创建人登录名称
     */
    private String createBy= BaseContextHandler.getUsername();

    /**
     * 创建日期
     */
    private LocalDateTime createTime=LocalDateTime.now();

    /**
     * 更新人登录名称
     */
    private String updateBy= BaseContextHandler.getUsername();

    /**
     * 更新日期
     */
    private LocalDateTime updateTime=LocalDateTime.now();


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
