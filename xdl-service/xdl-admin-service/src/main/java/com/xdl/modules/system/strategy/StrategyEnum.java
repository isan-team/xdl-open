package com.xdl.modules.system.strategy;

/**
 * 策略模式码表
 */
public enum StrategyEnum {

    /**
     * 淘宝
     */
    org_num_role("orgCodeRuleImpl");

    StrategyEnum(String className) {
        this.setClassName(className);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * class完整地址
     */
    private String className;

}
