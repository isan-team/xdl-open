package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysRolePermissionService extends IService<SysRolePermission> {

    boolean saveRolePermission(String roleId, String permissionIds, String lastPermissionIds);
}
