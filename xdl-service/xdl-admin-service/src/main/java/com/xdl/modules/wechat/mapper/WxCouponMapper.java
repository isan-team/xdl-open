package com.xdl.modules.wechat.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.wechat.entity.WxCoupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xdl.modules.wechat.vo.WxCouponVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-05
 */
public interface WxCouponMapper extends BaseMapper<WxCoupon> {

    IPage<WxCouponVO> queryPage(Page page, @Param("title") String title);
}
