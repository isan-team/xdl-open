package com.xdl.modules.wechat.service;

import com.xdl.modules.wechat.entity.WxOwnerCoupon;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-24
 */
public interface WxOwnerCouponService extends IService<WxOwnerCoupon> {

    /**
     * 获取过期
     * @return
     */
    List<WxOwnerCoupon> couponExpired();
}
