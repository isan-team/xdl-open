package com.xdl.modules.wechat.mapper;

import com.xdl.modules.wechat.entity.WxMyInvite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-24
 */
public interface WxMyInviteMapper extends BaseMapper<WxMyInvite> {

    /**
     *
     * @return
     */
    List<WxMyInvite> effectList();
}
