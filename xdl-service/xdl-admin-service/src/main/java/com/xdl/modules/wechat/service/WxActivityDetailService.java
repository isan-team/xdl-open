package com.xdl.modules.wechat.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xdl.modules.wechat.entity.WxActivityDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.wechat.model.ActivityModel;
import com.xdl.modules.wechat.vo.WxActivityDetailVO;

import java.util.List;

/**
 * <p>
 * 活动详情 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
public interface WxActivityDetailService extends IService<WxActivityDetail> {

    /**
     * 活动集合
     * @param model
     * @return
     */
    IPage<WxActivityDetailVO> queryPage(ActivityModel model);

    /**
     * 获取过期的活动
     * @return
     */
    List<WxActivityDetail> getExpiredActivity();
}
