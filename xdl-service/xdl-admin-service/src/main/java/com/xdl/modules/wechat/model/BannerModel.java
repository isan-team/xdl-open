package com.xdl.modules.wechat.model;

import com.xdl.modules.system.model.BasePage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BannerModel extends BasePage {

    private String actName;

}
