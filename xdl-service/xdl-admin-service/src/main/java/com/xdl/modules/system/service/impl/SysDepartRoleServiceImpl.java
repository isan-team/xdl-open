package com.xdl.modules.system.service.impl;

import com.xdl.modules.system.entity.SysDepartRole;
import com.xdl.modules.system.mapper.SysDepartRoleMapper;
import com.xdl.modules.system.service.SysDepartRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门角色表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
@Service
public class SysDepartRoleServiceImpl extends ServiceImpl<SysDepartRoleMapper, SysDepartRole> implements SysDepartRoleService {

}
