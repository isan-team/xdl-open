package com.xdl.modules.system.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.xdl.modules.system.service.FillRuleService;
import com.xdl.modules.system.strategy.FillRuleStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FillRuleServiceImpl implements FillRuleService {

    @Autowired
    private FillRuleStrategy fillRuleStrategy;


    @Override
    public Object executeRule(String ruleCode, JSONObject formData) {
        if (formData == null) {
            formData = new JSONObject();
        }
        return fillRuleStrategy.executeRule(ruleCode,formData);
    }
}
