package com.xdl.modules.system.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PhoneLoginModel {

    /**
     * 验证码
     */
    private String captcha;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 是否记住
     */
    private boolean remember_me;

}
