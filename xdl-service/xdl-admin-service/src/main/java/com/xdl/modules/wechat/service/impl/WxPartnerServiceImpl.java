package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.WxPartner;
import com.xdl.modules.wechat.mapper.WxPartnerMapper;
import com.xdl.modules.wechat.service.WxPartnerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
@Service
public class WxPartnerServiceImpl extends ServiceImpl<WxPartnerMapper, WxPartner> implements WxPartnerService {

}
