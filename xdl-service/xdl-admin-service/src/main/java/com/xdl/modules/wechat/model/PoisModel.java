package com.xdl.modules.wechat.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PoisModel {

    private String adname;

    private String pname;

    private String address;

    private String location;

    private String name;



}
