package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    List<String> getRoleByUserName(String username);
}
