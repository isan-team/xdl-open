package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysDepartPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门权限表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysDepartPermissionMapper extends BaseMapper<SysDepartPermission> {

}
