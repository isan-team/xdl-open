package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysAnnouncement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统通告表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-06-28
 */
public interface SysAnnouncementService extends IService<SysAnnouncement> {

    /**
     * 全部阅读操作
     * @return
     */
    int readAll();
}
