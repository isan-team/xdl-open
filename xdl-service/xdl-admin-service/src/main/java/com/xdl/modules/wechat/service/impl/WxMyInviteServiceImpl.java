package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.WxMyInvite;
import com.xdl.modules.wechat.mapper.WxMyInviteMapper;
import com.xdl.modules.wechat.service.WxMyInviteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-24
 */
@Service
public class WxMyInviteServiceImpl extends ServiceImpl<WxMyInviteMapper, WxMyInvite> implements WxMyInviteService {

    @Override
    public List<WxMyInvite> effectList() {
        return this.baseMapper.effectList();
    }
}
