package com.xdl.modules.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
@Controller
@RequestMapping("/sys-user-depart")
public class SysUserDepartController {

}
