package com.xdl.modules.wechat.mapper;

import com.xdl.modules.wechat.entity.WxBusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
public interface WxBusinessMapper extends BaseMapper<WxBusiness> {

}
