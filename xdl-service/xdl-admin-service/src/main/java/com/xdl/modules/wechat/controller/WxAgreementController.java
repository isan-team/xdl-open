package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.wechat.entity.WxAgreement;
import com.xdl.modules.wechat.model.AgreementModel;
import com.xdl.modules.wechat.service.WxAgreementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
@RestController
@RequestMapping("/wx/agreement")
public class WxAgreementController {

    @Autowired
    private WxAgreementService agreementService;

    @PostMapping("/list")
    public ResponseResult<?> list(@RequestBody AgreementModel model){
        QueryWrapper<WxAgreement> wrapper=new QueryWrapper<>();
        wrapper.lambda().like(StringUtils.isNotEmpty(model.getAgreementName()),WxAgreement::getAgreementName,model.getAgreementName())
                .orderByAsc(WxAgreement::getCreateTime);
        IPage<WxAgreement> page = this.agreementService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        return ResponseResult.Success(page);
    }

    @PostMapping("save")
    public ResponseResult<Boolean> save(@RequestBody WxAgreement entity){
        return ResponseResult.Success(this.agreementService.save(entity));
    }

    @PostMapping("edit")
    public ResponseResult<Boolean> edit(@RequestBody WxAgreement entity){
        entity.setUpdateTime(LocalDateTime.now());
        entity.setUpdateBy(BaseContextHandler.getUsername());
        return ResponseResult.Success(this.agreementService.updateById(entity));
    }

    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        boolean remove = this.agreementService.removeById(id);
        return ResponseResult.Success(remove);
    }

}
