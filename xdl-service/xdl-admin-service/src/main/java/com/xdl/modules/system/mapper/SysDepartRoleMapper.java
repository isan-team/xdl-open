package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysDepartRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门角色表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysDepartRoleMapper extends BaseMapper<SysDepartRole> {

}
