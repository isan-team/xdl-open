package com.xdl.modules.system.service;

import com.alibaba.fastjson.JSONObject;

public interface FillRuleService {

    Object executeRule(String depart, JSONObject formData);
}
