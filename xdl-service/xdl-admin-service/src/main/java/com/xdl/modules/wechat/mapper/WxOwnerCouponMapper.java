package com.xdl.modules.wechat.mapper;

import com.xdl.modules.wechat.entity.WxOwnerCoupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-24
 */
public interface WxOwnerCouponMapper extends BaseMapper<WxOwnerCoupon> {

    /**
     * 获取过期券
     * @return
     */
    List<WxOwnerCoupon> couponExpired();
}
