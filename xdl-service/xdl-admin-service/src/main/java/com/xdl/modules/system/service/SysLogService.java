package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统日志表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysLogService extends IService<SysLog> {

    Long findTotalVisitCount();

    Long findTodayVisitCount(Date dayStart, Date dayEnd);

    Long findTodayIp(Date dayStart, Date dayEnd);

    List<Map<String, Object>> findVisitCount(Date dayStart, Date dayEnd);
}
