package com.xdl.modules.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.response.ResponseResult;
import com.xdl.modules.system.entity.SysAnnouncement;
import com.xdl.modules.system.model.AnnouncementDto;
import com.xdl.modules.system.model.AnnouncementModel;
import com.xdl.modules.system.service.SysAnnouncementService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 系统通告表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-06-28
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/sys/annountCement")
public class SysAnnouncementController {

    private SysAnnouncementService announcementService;

    /**
     * @return
     * @功能：查询字典数据
     */
    @PostMapping("/list")
    public ResponseResult<AnnouncementDto> list(@RequestBody AnnouncementModel model) {
        QueryWrapper<SysAnnouncement> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(StringUtils.isNoneBlank(model.getTitle()), SysAnnouncement::getTitile, model.getTitle())
                .eq(StringUtils.isNoneBlank(model.getRead()),SysAnnouncement::getReadFlag,model.getRead())
                .orderByDesc(SysAnnouncement::getCreateTime);
        IPage<SysAnnouncement> iPage = announcementService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        int count = announcementService.count(wrapper);
        AnnouncementDto dto=new AnnouncementDto();
        dto.setResult(iPage.getRecords());
        dto.setCount(count);
        return ResponseResult.Success(dto);
    }


    /**
     * @return
     * @功能：查询字典数据
     */
    @PostMapping("/queryPageList")
    public ResponseResult<IPage<SysAnnouncement>> queryPageList(@RequestBody AnnouncementModel model) {
        QueryWrapper<SysAnnouncement> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(StringUtils.isNoneBlank(model.getTitle()), SysAnnouncement::getTitile, model.getTitle())
                .eq(StringUtils.isNoneBlank(model.getRead()),SysAnnouncement::getReadFlag,model.getRead())
                .orderByDesc(SysAnnouncement::getCreateTime);
        IPage<SysAnnouncement> iPage = announcementService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        return ResponseResult.Success(iPage);
    }

    @GetMapping("putRead")
    public ResponseResult<Boolean> putRead(@RequestParam Long id){
        SysAnnouncement announcement = announcementService.getById(id);
        announcement.setReadFlag(1);
        boolean b = announcementService.updateById(announcement);
        return ResponseResult.Success(b);
    }

    @GetMapping("readAll")
    public ResponseResult<Boolean> readAll(){
        int readAll = announcementService.readAll();
        return ResponseResult.Success(readAll>0);
    }



}
