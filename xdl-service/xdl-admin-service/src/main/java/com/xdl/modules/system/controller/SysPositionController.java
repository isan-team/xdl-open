package com.xdl.modules.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.modules.system.entity.SysPosition;
import com.xdl.modules.system.model.PositionModel;
import com.xdl.modules.system.service.SysPositionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-09
 */
@Slf4j
@RestController
@RequestMapping("/sys/position")
public class SysPositionController {

    @Autowired
    private SysPositionService positionService;

    @PostMapping(value = "/list")
    public ResponseResult<?> queryPageList(@RequestBody PositionModel model) {
        LambdaQueryWrapper<SysPosition> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.isNotEmpty(model.getCode()),SysPosition::getCode,model.getCode());
        wrapper.eq(StringUtils.isNotEmpty(model.getName()),SysPosition::getName,model.getName());
        wrapper.eq(StringUtils.isNotEmpty(model.getPostRank()),SysPosition::getPostRank,model.getPostRank());
        wrapper.orderByDesc(SysPosition::getCreateTime);
        IPage<SysPosition> page = positionService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        return ResponseResult.Success(page);
    }


    /**
     * 添加
     *
     * @param sysPosition
     * @return
     */
    @PostMapping(value = "/add")
    public ResponseResult<Boolean> add(@RequestBody SysPosition sysPosition) {
        boolean save = positionService.save(sysPosition);
        return ResponseResult.Success(save);
    }


    /**
     * 编辑
     *
     * @param sysPosition
     * @return
     */
    @PutMapping("/edit")
    public ResponseResult<Boolean> edit(@RequestBody SysPosition sysPosition) {
        SysPosition sysPositionEntity = positionService.getById(sysPosition.getId());
        Assert.notNull(sysPositionEntity,"未找到对应实体!");
        boolean result = positionService.updateById(sysPosition);
        return ResponseResult.Success(result);
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete")
    public ResponseResult<?> delete(@RequestParam(name = "id", required = true) String id) {
        boolean remove = positionService.removeById(id);
        return ResponseResult.Success(remove);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatch")
    public ResponseResult<Boolean> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        Assert.notNull(ids,"参数不识别！");
        boolean remove = this.positionService.removeByIds(Arrays.asList(ids.split(",")));
        return ResponseResult.Success(remove);
    }




}
