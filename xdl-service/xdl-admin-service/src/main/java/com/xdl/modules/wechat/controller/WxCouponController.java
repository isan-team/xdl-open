package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.wechat.entity.WxCoupon;
import com.xdl.modules.wechat.model.CouponModel;
import com.xdl.modules.wechat.service.WxCouponService;
import com.xdl.modules.wechat.vo.WxCouponVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-05
 */
@RestController
@RequestMapping("/wx/coupon")
public class WxCouponController {

    @Autowired
    private WxCouponService couponService;

    @PostMapping("/list")
    public ResponseResult<?> list(@RequestBody CouponModel model){
        IPage<WxCouponVO> page = this.couponService.queryPage(model);
        return ResponseResult.Success(page);
    }

    @PostMapping("save")
    public ResponseResult<Boolean> save(@RequestBody WxCoupon entity){
        return ResponseResult.Success(this.couponService.save(entity));
    }

    @PostMapping("edit")
    public ResponseResult<Boolean> edit(@RequestBody WxCoupon entity){
        entity.setUpdateTime(LocalDateTime.now());
        entity.setUpdateBy(BaseContextHandler.getUsername());
        return ResponseResult.Success(this.couponService.updateById(entity));
    }

    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        boolean remove = this.couponService.removeById(id);
        return ResponseResult.Success(remove);
    }



}
