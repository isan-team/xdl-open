package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.WxUser;
import com.xdl.modules.wechat.mapper.WxUserMapper;
import com.xdl.modules.wechat.service.WxUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 微信用户表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-16
 */
@Service
public class WxUserServiceImpl extends ServiceImpl<WxUserMapper, WxUser> implements WxUserService {

}
