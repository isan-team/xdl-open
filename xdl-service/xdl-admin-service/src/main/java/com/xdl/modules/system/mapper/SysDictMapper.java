package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xdl.modules.system.model.DuplicateCheckModel;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    Long duplicateCheckCountSql(DuplicateCheckModel model);

    Long duplicateCheckCountSqlNoDataId(DuplicateCheckModel model);

    @Select("select * from sys_dict where del_flag=1")
    List<SysDict> queryDeleteList();

    /**
     * 删除
     * @param id
     */
    @Delete("delete from sys_dict where id = #{id}")
    public void deleteOneById(@Param("id") String id);

    /**
     * 修改状态值
     * @param delFlag
     * @param id
     */
    @Update("update sys_dict set del_flag = #{flag,jdbcType=INTEGER} where id = #{id,jdbcType=VARCHAR}")
    public void updateDictDelFlag(@Param("flag") int delFlag, @Param("id") String id);
}
