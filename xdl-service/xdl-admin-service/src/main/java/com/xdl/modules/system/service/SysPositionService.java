package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysPosition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-09
 */
public interface SysPositionService extends IService<SysPosition> {

}
