package com.xdl.configuration;

import com.beluga.notice.sms.SMSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmsAutoConfiguration {

    @Autowired
    private SmsConfiguration configuration;

    @Bean
    public SMSClient smsClientBean(){
        final SMSClient bean=new SMSClient();
        bean.setAccessKey(configuration.getAccessKey());
        bean.setEndpoint(configuration.getEndpoint());
        bean.setSecretKey(configuration.getSecretKey());
        return bean;

    }


}
