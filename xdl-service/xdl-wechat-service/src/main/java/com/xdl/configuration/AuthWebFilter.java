package com.xdl.configuration;

import com.alibaba.fastjson.JSONObject;
import com.beluga.core.util.Func;
import com.xdl.common.jwt.IJWTInfo;
import com.xdl.common.jwt.JWTHelper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;


/**
 * @ClassName TransmitUserInfoFilter
 * @Description TODO
 * @Author iron
 * @Date 2020/8/31 13:50
 * @Version 1.0
 */
@Slf4j
@Configuration
public class AuthWebFilter implements Filter {

    private static final String[] AUTH_WHITELIST = {
            "/wx/login/loginWithWechat",
            "/wx/activity/list",
            "/wx/activity/getActById",
            "/wx/activity/getActClassify",
            "/wx/banner/list",
            "/wx/business/list",
            "/wx/coupon/list",
            "/wx/partner/list",
            "/wx/order/callback",
            "/wx/order/refundCallback",
            "/wx/shop/list",
            "/wx/invite/getInviteCode",
            "/wx/filter/getFilterData",
            "/wx/search/searchEngine",
            "/wx/search/searchEngine",
            "/wx/activity/hotList",
            "/wx/filter/getFilterData",
            "/wx/agreement/getAggreement"
    };


    public AuthWebFilter() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @SneakyThrows
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        String requestURI = ((HttpServletRequest) request).getRequestURI();
        //权限校验
        if (!Arrays.asList(AUTH_WHITELIST).contains(requestURI)) {
            //获取token
            String token = ((HttpServletRequest) request).getHeader("token");
            if (Func.isNotEmpty(token) && JWTHelper.verify(token)) {
                IJWTInfo jwt = JWTHelper.getInfoFromToken(token);
                BaseContextHandler.setUsername(jwt.getUserName());
                BaseContextHandler.setUserID(jwt.getUserId());
                BaseContextHandler.setPhone(jwt.getPhone());
                BaseContextHandler.setOpenId(jwt.getOpenId());
                chain.doFilter(request, response);
            } else {
                BaseContextHandler.remove();
                unauthorized(response, "接口未被授权", 40029);
            }
        } else {
            String token = ((HttpServletRequest) request).getHeader("token");
            if (Func.isNotEmpty(token) && JWTHelper.verify(token)) {
                IJWTInfo jwt = JWTHelper.getInfoFromToken(token);
                BaseContextHandler.setUsername(jwt.getUserName());
                BaseContextHandler.setUserID(jwt.getUserId());
                BaseContextHandler.setPhone(jwt.getPhone());
                BaseContextHandler.setOpenId(jwt.getOpenId());
            }
            chain.doFilter(request, response);
        }
    }



    public void unauthorized(ServletResponse res, String msg, Integer code) throws IOException, ServletException {
        JSONObject obj = new JSONObject();
        obj.put("code", code);
        obj.put("data", null);
        obj.put("message", msg);
        ServletOutputStream outputStream = res.getOutputStream();
        outputStream.write(obj.toJSONString().getBytes("UTF-8"));
        outputStream.flush();
        outputStream.close();
    }

    @Override
    public void destroy() {
    }

}
