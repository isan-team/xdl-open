package com.xdl.modules.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xdl.modules.dto.WxActivityDto;
import com.xdl.modules.entity.WxActivitySave;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.model.BasePage;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-22
 */
public interface WxActivitySaveService extends IService<WxActivitySave> {

    /**
     * 获取收藏列表
     * @param page
     * @return
     */
    IPage<WxActivityDto> getSaveList(BasePage page);
}
