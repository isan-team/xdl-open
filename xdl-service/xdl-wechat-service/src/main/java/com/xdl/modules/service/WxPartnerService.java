package com.xdl.modules.service;

import com.xdl.modules.entity.WxPartner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-25
 */
public interface WxPartnerService extends IService<WxPartner> {

}
