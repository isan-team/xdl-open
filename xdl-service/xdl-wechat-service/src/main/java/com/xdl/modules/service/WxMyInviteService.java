package com.xdl.modules.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.dto.InviteDto;
import com.xdl.modules.entity.WxMyInvite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-13
 */
public interface WxMyInviteService extends IService<WxMyInvite> {

    /**
     * 获取我的邀请列表
     * @param objectPage
     * @param userid
     * @return
     */
    IPage<InviteDto> getMyInviteList(Page<Object> objectPage, Long userid);
}
