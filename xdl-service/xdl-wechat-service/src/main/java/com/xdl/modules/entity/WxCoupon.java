package com.xdl.modules.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxCoupon extends Model<WxCoupon> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 优惠券标题
     */
    private String title;

    /**
     * 优惠券金额
     */
    private Integer money;

    /**
     * 使用商铺
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long shopId;

    /**
     * 使用规则
     */
    private String rule;

    /**
     * 使用方式
     */
    private String ruleRemark;

    /**
     * 开始时间
     */
    private LocalDateTime startDay;

    /**
     * 截止时间
     */
    private LocalDateTime endDay;


    /**
     * 有效期（day）
     */
    private Integer validity;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新人
     */
    private String updateBy;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
