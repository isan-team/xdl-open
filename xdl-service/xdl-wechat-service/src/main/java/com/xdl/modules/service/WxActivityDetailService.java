package com.xdl.modules.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xdl.modules.dto.ActivityClassifyDto;
import com.xdl.modules.dto.WxActivityDto;
import com.xdl.modules.entity.WxActivityDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.model.ActivityModel;

import java.util.List;

/**
 * <p>
 * 活动详情 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-20
 */
public interface WxActivityDetailService extends IService<WxActivityDetail> {

    /**
     * 活动列表
     * @param model
     * @return
     */
    IPage<WxActivityDto> getList(ActivityModel model);

    /**
     * 根据id获取活动信息
     * @param id
     * @return
     */
    WxActivityDto getActById(Long id);

    /**
     * 获取活动分类
     * @return
     */
    List<ActivityClassifyDto> getActClassify();
}
