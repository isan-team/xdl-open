package com.xdl.modules.service;

import com.xdl.modules.entity.WxBusiness;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-22
 */
public interface WxBusinessService extends IService<WxBusiness> {

}
