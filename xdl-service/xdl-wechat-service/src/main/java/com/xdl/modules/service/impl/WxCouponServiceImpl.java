package com.xdl.modules.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.beluga.core.util.Func;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.dto.WxCouponDto;
import com.xdl.modules.entity.WxCoupon;
import com.xdl.modules.mapper.WxCouponMapper;
import com.xdl.modules.model.BasePage;
import com.xdl.modules.service.WxCouponService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-26
 */
@Service
public class WxCouponServiceImpl extends ServiceImpl<WxCouponMapper, WxCoupon> implements WxCouponService {

    /**
     * 获取优惠券列表
     * @param page
     * @return
     */
    @Override
    public List<WxCouponDto> getCouponList(BasePage page) {
        Long userid= Func.isNotEmpty(BaseContextHandler.getUserID()) ? Long.valueOf(BaseContextHandler.getUserID()) : null;
        return this.baseMapper.getCouponList(new Page<>(page.getPageNo(), page.getPageSize()),userid);
    }
}
