package com.xdl.modules.service.impl;

import com.xdl.modules.entity.WxBusiness;
import com.xdl.modules.mapper.WxBusinessMapper;
import com.xdl.modules.service.WxBusinessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-22
 */
@Service
public class WxBusinessServiceImpl extends ServiceImpl<WxBusinessMapper, WxBusiness> implements WxBusinessService {

}
