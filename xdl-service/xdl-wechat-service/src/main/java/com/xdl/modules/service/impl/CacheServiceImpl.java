package com.xdl.modules.service.impl;

import com.xdl.common.utils.RedisUtils;
import com.xdl.modules.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class CacheServiceImpl implements CacheService {

    @Autowired
    private RedisUtils cache;




    @Override
    public void delKey(String realKey) {
       cache.del(realKey);
    }

    @Override
    public boolean setCache(String key, String value, long time) {
        return cache.set(key, value, time);
    }

    @Override
    public void setOrderCache(String key, Map<String, String> payinfo, long time) {
        Map<String, Object> pay = (HashMap<String, Object>)(Map)payinfo;
        cache.hmset(key,pay,time);
    }

    @Override
    public Map<Object, Object> getMapCache(String key) {
        return  cache.hmget(key);
    }


}
