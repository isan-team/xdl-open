package com.xdl.modules.controller;


import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseCollection;
import com.beluga.core.response.ResponseResult;
import com.xdl.modules.entity.WxUserTemp;
import com.xdl.modules.service.WxUserTempService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-30
 */
@AllArgsConstructor
@BelugaController("/wx/usertemp")
public class WxUserTempController {

    private WxUserTempService tempService;


    @GetMapping("getTemp")
    public ResponseCollection<WxUserTemp> getTemp(){
        List<WxUserTemp> result=tempService.pullTemp();
        return ResponseCollection.Success(result);
    }



    @DeleteMapping("remove")
    public ResponseResult<Boolean> remove(@RequestParam Long id){
        boolean result= tempService.removeTemp(id);
        return ResponseResult.Success(result);
    }


}
