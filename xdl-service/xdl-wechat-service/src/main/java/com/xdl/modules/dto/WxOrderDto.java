/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.modules.util.RelativeDateFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author iron.guo
 * @Date 2022/5/1
 * @Description
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxOrderDto {

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long actId;
    /**
     * 订单号
     */
    private String orderSn;

    /**
     * 支付订单号
     */
    private String transactionId;

    /**
     * 活动名称
     */
    private String title;

    /**
     * 活动分类
     */
    private String classify;

    /**
     * 主图
     */
    private String mainPic;

    /**
     * 活动内容
     */
    private String actName;

    /**
     * 商户名称
     */
    private String shopName;

    /**
     * 支付金额
     */
    private BigDecimal price;

    /**
     * 购买数量
     */
    private Integer orderNum;


    /**
     * 购买数量
     */
    private Integer needPay;

    /**
     * 支付状态 支付状态1-待支付2-已支付3-取消支付 4-退款 5-过期支付
     */
    private Integer payStatus;

    /**
     * 状态 状态1-待付款2-待使用 3-已使用4-售后 5-失效订单
     */
    private Integer status;

    private long between;

    /**
     * 预约时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date appointTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;


    public long getBetween() {
        return RelativeDateFormat.betweenTime(this.createTime);
    }
}
