package com.xdl.modules.mapper;

import com.xdl.modules.entity.WxPartner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-25
 */
public interface WxPartnerMapper extends BaseMapper<WxPartner> {

}
