package com.xdl.modules.mapper;

import com.xdl.modules.entity.WxBusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-22
 */
public interface WxBusinessMapper extends BaseMapper<WxBusiness> {

}
