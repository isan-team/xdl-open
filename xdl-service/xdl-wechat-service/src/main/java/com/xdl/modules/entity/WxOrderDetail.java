package com.xdl.modules.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.modules.constant.OrderEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-04-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxOrderDetail extends BaseEntity<WxOrderDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 订单号
     */
    private String orderSn;

    /**
     * 支付令牌
     */
    private String transactionId;

    /**
     * 活动id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long actId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 紧急联系人
     */
    private String contacts;

    /**
     * 血型
     */
    private Integer blood;

    /**
     * 预约地点
     */
    private String address;

    /**
     * 优惠券id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long couponId;

    /**
     * 预约日期
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date appointTime;

    /**
     * 是否参保
     */
    private Boolean insurance;

    /**
     * 支付金额
     */
    private BigDecimal price;

    /**
     * 购买数量
     */
    private Integer orderNum;

    /**
     * 支付状态
     */
    private Integer payStatus= OrderEnum.STAY_PAY;

    /**
     * 状态
     */
    private Integer status=OrderEnum.STAY_BPAY;

}
