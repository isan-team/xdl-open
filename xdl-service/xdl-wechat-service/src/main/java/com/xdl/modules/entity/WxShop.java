package com.xdl.modules.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商户表
 * </p>
 *
 * @author iron guo
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxShop extends BaseEntity<WxShop> {

    private static final long serialVersionUID = 1L;

    /**
     * 商户名称
     */
    private String shopName;

    /**
     * 商户icon
     */
    private String shopIcon;

    /**
     * 商户所在城市
     */
    private String city;

    /**
     * 商户地址
     */
    private String adress;


    /**
     * 经纬度
     */
    private String location;

}
