package com.xdl.modules.controller;


import com.beluga.core.annotation.BLimit;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseCollection;
import com.beluga.core.response.ResponseResult;
import com.xdl.modules.service.WxBusinessService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-22
 */
@AllArgsConstructor
@BelugaController("/wx/business")
public class WxBusinessController {

    private WxBusinessService businessService;

    /**
     * 获取商务合作列表
     * @return
     */
    @BLimit(value ="businessList",limit = 10)
    @GetMapping("list")
    public ResponseResult list(){
        return ResponseResult.Success(businessService.list());
    }

}
