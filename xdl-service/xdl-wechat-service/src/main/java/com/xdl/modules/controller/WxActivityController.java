package com.xdl.modules.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.beluga.core.annotation.BLimit;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseCollection;
import com.beluga.core.response.ResponseResult;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.dto.ActivityClassifyDto;
import com.xdl.modules.dto.WxActivityDto;
import com.xdl.modules.entity.WxActivityDetail;
import com.xdl.modules.entity.WxActivitySave;
import com.xdl.modules.model.ActivityModel;
import com.xdl.modules.model.BasePage;
import com.xdl.modules.service.WxActivityDetailService;
import com.xdl.modules.service.WxActivitySaveService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 活动详情 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-20
 */
@AllArgsConstructor
@BelugaController("/wx/activity")
public class WxActivityController {

    private WxActivityDetailService activityService;

    private WxActivitySaveService activitySaveService;

    /**
     * 获取新动力发布活动列表
     *
     * @param model
     * @return
     */
    @BLimit(value = "ActList", limit = 10)
    @PostMapping("list")
    public ResponseResult<IPage<WxActivityDto>> list(@RequestBody ActivityModel model) {
        return ResponseResult.Success(this.activityService.getList(model));
    }

    /**
     * 获取新动力热门活动
     * @param
     * @return
     */
    @GetMapping("hotList")
    public ResponseCollection<WxActivityDetail> hotlist() {
        QueryWrapper<WxActivityDetail> wrapper = new QueryWrapper<>();
        wrapper.lambda().orderByDesc(WxActivityDetail::getCreateTime);
        IPage<WxActivityDetail> page = this.activityService.page(new Page<>(1, 3), wrapper);
        return ResponseCollection.Success(page.getRecords());
    }


    /**
     * 根据id获取活动详情
     *
     * @param id
     * @return
     */
    @GetMapping("getActById")
    public ResponseResult getActById(@RequestParam Long id) {
        return ResponseResult.Success(this.activityService.getActById(id));
    }

    /**
     * 收藏活动
     *
     * @param id
     * @return
     */
    @GetMapping("save")
    public ResponseResult<Boolean> save(@RequestParam Long id) {
        QueryWrapper<WxActivitySave> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(WxActivitySave::getActId, id).eq(WxActivitySave::getUserId, Long.parseLong(BaseContextHandler.getUserID()));
        int count = this.activitySaveService.count(wrapper);
        if (count > 0) {
            return ResponseResult.Success(true);
        } else {
            WxActivitySave save = new WxActivitySave();
            save.setActId(id);
            save.setUserId(Long.parseLong(BaseContextHandler.getUserID()));
            boolean result = activitySaveService.save(save);
            return ResponseResult.Success(result);
        }

    }


    /**
     * 取消收藏
     *
     * @param id
     * @return
     */
    @GetMapping("unsave")
    public ResponseResult<Boolean> unsave(@RequestParam Long id) {
        QueryWrapper<WxActivitySave> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(WxActivitySave::getActId, id).eq(WxActivitySave::getUserId, Long.parseLong(BaseContextHandler.getUserID()));
        boolean remove = activitySaveService.remove(wrapper);
        return ResponseResult.Success(remove);
    }


    /**
     * 获取活动收藏列表
     *
     * @param page
     * @return
     */
    @PostMapping("getSaveList")
    public ResponseResult<IPage<WxActivityDto>> getSaveList(@RequestBody BasePage page) {
        return ResponseResult.Success(activitySaveService.getSaveList(page));
    }

    /**
     * 获取活动分类
     *
     * @return
     */
    @GetMapping("getActClassify")
    public ResponseCollection<ActivityClassifyDto> getActClassify() {
        List<ActivityClassifyDto> result = activityService.getActClassify();
        return ResponseCollection.Success(result);
    }


}
