package com.xdl.modules.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.dto.WxActivityDto;
import com.xdl.modules.entity.WxActivitySave;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-22
 */
public interface WxActivitySaveMapper extends BaseMapper<WxActivitySave> {

    IPage<WxActivityDto> getSaveList(Page page,@Param("userid") Long userId);
}
