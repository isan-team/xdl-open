package com.xdl.modules.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WechatLoginDto {

    private String code;

    private String nickname;

    private String headImgUrl;

    private String encryptedData;

    private String ivStr;




}
