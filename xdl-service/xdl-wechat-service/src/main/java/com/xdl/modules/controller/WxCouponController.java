package com.xdl.modules.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.beluga.core.annotation.BLimit;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseCollection;
import com.beluga.core.response.ResponseResult;
import com.xdl.modules.dto.WxCouponDto;
import com.xdl.modules.entity.WxCoupon;
import com.xdl.modules.model.BasePage;
import com.xdl.modules.service.WxCouponService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-26
 */
@AllArgsConstructor
@BelugaController("/wx/coupon")
public class WxCouponController {

    private WxCouponService couponService;

    @BLimit(value ="couponList",limit = 10)
    @PostMapping("list")
    public ResponseResult<?> list(@RequestBody BasePage page){
        List<WxCouponDto> ipage = couponService.getCouponList(page);
        return ResponseResult.Success(ipage);
    }




}
