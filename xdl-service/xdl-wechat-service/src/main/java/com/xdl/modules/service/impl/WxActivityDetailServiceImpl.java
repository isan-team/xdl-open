package com.xdl.modules.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.beluga.core.util.Func;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.dto.ActivityClassifyDto;
import com.xdl.modules.dto.WxActivityDto;
import com.xdl.modules.entity.WxActivityDetail;
import com.xdl.modules.mapper.WxActivityDetailMapper;
import com.xdl.modules.model.ActivityModel;
import com.xdl.modules.service.WxActivityDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 活动详情 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-20
 */
@Service
public class WxActivityDetailServiceImpl extends ServiceImpl<WxActivityDetailMapper, WxActivityDetail> implements WxActivityDetailService {

    /**
     * 获取活动列表
     *
     * @param model
     * @return
     */
    @Override
    public IPage<WxActivityDto> getList(ActivityModel model) {
        IPage<WxActivityDto> iPage = this.baseMapper.getList(new Page<>(model.getPageNo(), model.getPageSize()), model);
        return iPage;
    }

    /**
     * 根据id获取活动信息
     *
     * @param id
     * @return
     */
    @Override
    public WxActivityDto getActById(Long id) {
        String uid = BaseContextHandler.getUserID();
        Long userid = Func.isNotBlank(uid) ? Long.valueOf(BaseContextHandler.getUserID()) : 0l;
        return this.baseMapper.getActById(id, userid);
    }

    @Override
    public List<ActivityClassifyDto> getActClassify() {
        return this.baseMapper.getActClassify();
    }
}
