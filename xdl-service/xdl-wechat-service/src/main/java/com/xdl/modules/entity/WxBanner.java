package com.xdl.modules.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * banner图表
 * </p>
 *
 * @author iron guo
 * @since 2022-04-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxBanner extends BaseEntity<WxBanner> {

    private static final long serialVersionUID = 1L;


    /**
     * banner名称
     */
    private String title;

    /**
     * banner图地址
     */
    private String bannerUrl;

    /**
     * 活动id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long actId;

}
