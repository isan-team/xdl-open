package com.xdl.modules.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.beluga.core.annotation.BelugaController;
import com.xdl.common.response.ResponseResult;
import com.xdl.modules.entity.WxAgreement;
import com.xdl.modules.service.WxAgreementService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-05-26
 */
@AllArgsConstructor
@BelugaController("/wx/agreement")
public class WxAgreementController {

    private WxAgreementService agreementService;

    @GetMapping("getAggreement")
    public ResponseResult<WxAgreement> getAggreement(@RequestParam String type){
        QueryWrapper<WxAgreement> wrapper=new QueryWrapper<>();
        wrapper.lambda().eq(WxAgreement::getAgreementCode,type);
        WxAgreement agreement = agreementService.getOne(wrapper);
        return ResponseResult.Success(agreement);
    }



}
