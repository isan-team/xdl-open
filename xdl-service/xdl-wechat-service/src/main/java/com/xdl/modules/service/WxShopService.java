package com.xdl.modules.service;

import com.xdl.modules.dto.KV;
import com.xdl.modules.dto.WxShopDto;
import com.xdl.modules.entity.WxShop;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商户表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-10
 */
public interface WxShopService extends IService<WxShop> {

    /**
     * 根据经纬度获取商户列表以及距离
     * @param lat
     * @param lng
     * @return
     */
    List<WxShopDto> getList(String lat, String lng);

    /**
     * 获取商户列表
     * @return
     */
    List<KV> getShopList();
}
