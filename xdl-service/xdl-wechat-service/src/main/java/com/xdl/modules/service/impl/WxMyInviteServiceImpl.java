package com.xdl.modules.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.dto.InviteDto;
import com.xdl.modules.entity.WxMyInvite;
import com.xdl.modules.mapper.WxMyInviteMapper;
import com.xdl.modules.service.WxMyInviteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-13
 */
@Service
public class WxMyInviteServiceImpl extends ServiceImpl<WxMyInviteMapper, WxMyInvite> implements WxMyInviteService {


    @Override
    public IPage<InviteDto> getMyInviteList(Page<Object> page, Long userid) {
        return this.baseMapper.getMyInviteList(page,userid);
    }
}
