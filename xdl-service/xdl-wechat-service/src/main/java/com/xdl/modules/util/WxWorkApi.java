/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.util;

import com.alibaba.fastjson.JSONObject;
import com.xdl.common.utils.RestUtil;
import com.xdl.modules.model.OfferContent;
import com.xdl.modules.model.OfferModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author iron.guo
 * @Date 2022/6/28
 * @Description
 */
@Component
public class WxWorkApi {

    @Value("${wxwork.corpid}")
    private String corpid;

    @Value("${wxwork.corpsecret}")
    private String corpsecret;

    /**
     * 获取企业微信tonken
     *
     * @param
     * @return
     */
    public String getWechatCpToken() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("corpid", corpid);
        jsonObject.put("corpsecret", corpsecret);
        JSONObject result = RestUtil.get(UrlConstant.WxWorkToken, jsonObject);
        return result.get("access_token").toString();
    }



    public boolean sendWxWorkOffer(String msg){
        String token=getWechatCpToken();
        OfferModel model=new OfferModel();
        model.setText(new OfferContent().setContent(msg));
        JSONObject body= (JSONObject) JSONObject.toJSON(model);
        JSONObject v=new JSONObject();
        v.put("access_token",token);
        RestUtil.post(UrlConstant.sendWxWorkOffer,v,body);
        return true;
    }

}
