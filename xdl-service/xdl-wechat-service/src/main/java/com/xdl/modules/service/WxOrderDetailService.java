package com.xdl.modules.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xdl.modules.dto.WxOrderDto;
import com.xdl.modules.dto.WxOrderToken;
import com.xdl.modules.entity.WxOrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.model.WxOrderModel;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-30
 */
public interface WxOrderDetailService extends IService<WxOrderDetail> {

    /**
     * 下单预约活动
     * @param order
     * @return
     */
    WxOrderToken order(WxOrderDetail order);

    /**
     * 获取订单列表
     * @param model
     * @return
     */
    IPage<WxOrderDto> getOrderList(WxOrderModel model);

    /**
     * 取消订单
     * @param id
     * @return
     */
    String cancelOrder(Long id);

    /**
     * 关闭订单
     * @param id
     * @return
     */
    String closeOrder(Long id);

    /**
     * 支付订单
     * @param id
     * @param orderSn
     * @return
     */
    Map<Object,Object> payOrder(String orderSn);
}
