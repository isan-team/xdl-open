package com.xdl.modules.service.impl;

import com.xdl.modules.dto.KV;
import com.xdl.modules.dto.WxShopDto;
import com.xdl.modules.entity.WxShop;
import com.xdl.modules.mapper.WxShopMapper;
import com.xdl.modules.service.WxShopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商户表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-10
 */
@Service
public class WxShopServiceImpl extends ServiceImpl<WxShopMapper, WxShop> implements WxShopService {

    /**
     *
     * @param lat 0—90度
     * @param lng -180~180
     * @return
     */
    @Override
    public List<WxShopDto> getList(String lat, String lng) {
        return this.baseMapper.getList(lat,lng);
    }

    @Override
    public List<KV> getShopList() {
        return this.baseMapper.getShopList();
    }
}
