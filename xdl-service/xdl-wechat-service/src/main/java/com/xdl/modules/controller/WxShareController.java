/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.controller;

import cn.hutool.core.codec.Base64;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseCollection;
import com.beluga.core.response.ResponseResult;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.dto.InviteDto;
import com.xdl.modules.entity.WxMyInvite;
import com.xdl.modules.entity.WxOrderDetail;
import com.xdl.modules.service.WxMiniService;
import com.xdl.modules.service.WxMyInviteService;
import com.xdl.modules.service.WxOrderDetailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @Author iron.guo
 * @Date 2022/5/10
 * @Description
 */
@Slf4j
@AllArgsConstructor
@BelugaController("/wx/invite")
public class WxShareController {

    private WxMiniService miniService;

    private WxMyInviteService inviteService;

    private WxOrderDetailService orderService;

    /**
     * 获取微信小程序邀请二维码
     * @return
     */
    @GetMapping("getInviteCode")
    public ResponseResult<?> getInviteCode() {
        String scene = "userid=" + BaseContextHandler.getUserID();
        String page = "pages/index/index";
        String envVersion = "release";
        byte[] wxbyte = miniService.getWxQrcode(scene, page, envVersion);
        String encode = Base64.encode(wxbyte);
        return ResponseResult.Success(encode);
    }


    /**
     * 获取我的邀请列表
     *
     * @return @{link WxMyInvite}
     */
    @GetMapping("getMyInviteList")
    public ResponseCollection<InviteDto> getMyInviteList(){
        Long userid= Long.valueOf(BaseContextHandler.getUserID());
        IPage<InviteDto> page=inviteService.getMyInviteList(new Page<>(1,5),userid);
        return ResponseCollection.Success(page.getRecords());
    }


    /**
     * 邀请用户
     * @param userid
     * @return
     */
    @GetMapping("inviteUser")
    public ResponseResult<?> inviteUser(@RequestParam Long userid) {
        QueryWrapper<WxMyInvite> wrapper=new QueryWrapper<>();
        wrapper.lambda().eq(WxMyInvite::getUserId,Long.valueOf(BaseContextHandler.getUserID()));
        WxMyInvite myInvite = inviteService.getOne(wrapper);
        if(userid.equals(Long.valueOf(BaseContextHandler.getUserID()))){
            log.info("自己不能邀请自己");
            return ResponseResult.Success();
        }
        QueryWrapper<WxOrderDetail> owrapper=new QueryWrapper<>();
        owrapper.lambda().eq(WxOrderDetail::getUserId,Long.valueOf(BaseContextHandler.getUserID()));
        int count = orderService.count(owrapper);
        if(count>0){
            log.info("该用户为老用户");
            return ResponseResult.Success();
        }
        if(myInvite==null){
            WxMyInvite invite=new WxMyInvite();
            invite.setUserId(Long.valueOf(BaseContextHandler.getUserID()));
            invite.setInviteId(userid);
            invite.setEffect(0);
            inviteService.save(invite);
        }
        return ResponseResult.Success();
    }



}
