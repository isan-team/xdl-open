package com.xdl.modules.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-04-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxPartner extends BaseEntity<WxPartner> {

    private static final long serialVersionUID = 1L;

    /**
     * 合作方名称
     */
    private String partnerName;

    /**
     * 推广图
     */
    private String partnerBanner;

    /**
     * 备注
     */
    private String remark;

}
