package com.xdl.modules.service.impl;

import com.xdl.modules.entity.WxBanner;
import com.xdl.modules.mapper.WxBannerMapper;
import com.xdl.modules.service.WxBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * banner图表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-21
 */
@Service
public class WxBannerServiceImpl extends ServiceImpl<WxBannerMapper, WxBanner> implements WxBannerService {

}
