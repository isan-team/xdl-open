package com.xdl.modules.controller;


import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseResult;
import com.beluga.core.util.Func;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.dto.UserDto;
import com.xdl.modules.entity.WxUser;
import com.xdl.modules.service.WxUserService;
import lombok.AllArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 微信用户表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-21
 */
@AllArgsConstructor
@BelugaController("/wx/user")
public class WxUserController {

    private WxUserService userService;

    /**
     * 根据id获取用户信息
     * @param
     * @return
     */
    @GetMapping("getUserInfo")
    public ResponseResult<UserDto> getUserInfo(){
        WxUser wxUser = userService.getById(BaseContextHandler.getUserID());
        UserDto result=new UserDto();
        /**
         * 脱敏
         */
        Func.copy(wxUser,result);
        return ResponseResult.Success(result);
    }

    /**
     * 编辑用户信息
     * @param dto
     * @return
     */
    @PostMapping("edit")
    public ResponseResult<WxUser> edit(@RequestBody UserDto dto){
        WxUser wxUser = userService.getById(dto.getId());
        Assert.notNull(wxUser,"用户信息不存在");
        Func.copy(dto,wxUser);
        userService.updateById(wxUser);
        return ResponseResult.Success(wxUser);
    }










}
