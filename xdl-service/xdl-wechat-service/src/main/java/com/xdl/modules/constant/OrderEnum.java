/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.constant;

/**
 * @Author iron.guo
 * @Date 2022/5/6
 * @Description 订单&支付类码表
 */

public class OrderEnum {

    /**############# 支付码表 ####################**/

    /**
     * 支付状态1-待支付2-已支付3-取消支付 4-退款 5-过期支付
     */
    public static final Integer STAY_PAY = 1;

    /**
     * 支付状态1-待支付2-已支付3-取消支付 4-退款 5-过期支付
     */
    public static final Integer IS_PAY = 2;

    /**
     * 支付状态1-待支付2-已支付3-取消支付 4-退款 5-过期支付
     */
    public static final Integer CANLE_PAY = 3;

    /**
     * 支付状态1-待支付2-已支付3-取消支付 4-退款 5-过期支付
     */
    public static final Integer REFUND = 4;

    /**
     * 支付状态1-待支付2-已支付3-取消支付 4-退款 5-过期支付
     */
    public static final Integer OVERDUE_PAY = 5;

    /**################ 订单码表 #################**/

    /**
     * 状态 状态1-待付款2-待使用 3-已使用4-售后 5-失效订单
     */
    public static final Integer STAY_BPAY = 1;

    /**
     * 状态 状态1-待付款2-待使用 3-已使用4-售后 5-失效订单
     */
    public static final Integer IS_BPAY = 2;

    /**
     * 状态 状态1-待付款2-待使用 3-已使用4-售后 5-失效订单
     */
    public static final Integer IS_USE = 3;

    /**
     * 状态 状态1-待付款2-待使用 3-已使用4-售后 5-失效订单
     */
    public static final Integer AFTER_SALE = 4;

    /**
     * 状态 状态1-待付款2-待使用 3-已使用4-售后 5-失效订单
     */
    public static final Integer LOST_EFFICACY = 5;


}
