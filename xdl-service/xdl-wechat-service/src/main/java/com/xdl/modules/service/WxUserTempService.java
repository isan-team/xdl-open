package com.xdl.modules.service;

import com.xdl.modules.entity.WxUserTemp;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-30
 */
public interface WxUserTempService extends IService<WxUserTemp> {

    /**
     * 保存用户信息模版
     * @param temp
     * @return
     */
    WxUserTemp putUser(WxUserTemp temp);

    /**
     * 拉取常用用户信息
     * @return
     */
    List<WxUserTemp> pullTemp();

    /**
     * 删除用户常用信息
     * @param id
     * @return
     */
    Boolean removeTemp(Long id);
}
