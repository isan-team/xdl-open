package com.xdl.modules.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxBusiness extends BaseEntity<WxBusiness> {

    private static final long serialVersionUID = 1L;

    /**
     * 合作意向
     */
    private String business;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 联系人名字
     */
    private String name;

    /**
     * 微信二维码
     */
    private String wxEwm;

}
