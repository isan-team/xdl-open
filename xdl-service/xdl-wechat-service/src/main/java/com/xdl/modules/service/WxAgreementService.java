package com.xdl.modules.service;

import com.xdl.modules.entity.WxAgreement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-26
 */
public interface WxAgreementService extends IService<WxAgreement> {

}
