package com.xdl.modules.service;

import com.xdl.modules.entity.WxBanner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * banner图表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-21
 */
public interface WxBannerService extends IService<WxBanner> {

}
