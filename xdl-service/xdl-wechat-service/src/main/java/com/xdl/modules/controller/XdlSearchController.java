/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.beluga.core.annotation.BLimit;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseCollection;
import com.beluga.core.response.ResponseResult;
import com.xdl.modules.dto.WxActivityDto;
import com.xdl.modules.entity.WxActivityDetail;
import com.xdl.modules.model.ActivityModel;
import com.xdl.modules.service.WxActivityDetailService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author iron.guo
 * @Date 2022/5/13
 * @Description
 */
@AllArgsConstructor
@BelugaController("/wx/search")
public class XdlSearchController {

    private WxActivityDetailService activityService;

    /**
     * 新动力活动搜索引擎
     * @param keyword
     * @return
     */
    @PostMapping("searchEngine")
    @BLimit(value ="searchEngine",limit = 10)
    public ResponseResult<?> searchEngine(@RequestBody ActivityModel model){
//        QueryWrapper<WxActivityDetail> wrapper=new QueryWrapper<>();
//        wrapper.lambda().like(WxActivityDetail::getTitle,keyword).orderByDesc(WxActivityDetail::getCreateTime);
//        List<WxActivityDetail> result = activityService.list(wrapper);

        IPage<WxActivityDto> list = this.activityService.getList(model);

        return ResponseResult.Success(list.getRecords());
    }

}
