package com.xdl.modules.mapper;

import com.xdl.modules.entity.WxInsurance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-09
 */
public interface WxInsuranceMapper extends BaseMapper<WxInsurance> {

}
