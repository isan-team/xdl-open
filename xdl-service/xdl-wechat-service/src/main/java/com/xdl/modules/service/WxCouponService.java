package com.xdl.modules.service;

import com.xdl.modules.dto.WxCouponDto;
import com.xdl.modules.entity.WxCoupon;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.model.BasePage;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-26
 */
public interface WxCouponService extends IService<WxCoupon> {

    /**
     * 获取
     * @param page
     * @return
     */
    List<WxCouponDto> getCouponList(BasePage page);
}
