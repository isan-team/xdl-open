package com.xdl.modules.mapper;

import com.xdl.modules.dto.KV;
import com.xdl.modules.dto.WxShopDto;
import com.xdl.modules.entity.WxShop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商户表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-10
 */
public interface WxShopMapper extends BaseMapper<WxShop> {

    /**
     *
     * @param lat
     * @param lng
     * @return
     */
    List<WxShopDto> getList(@Param("lat") String lat,@Param("lng") String lng);

    /**
     * 获取商户列表
     * @return
     */
    List<KV> getShopList();
}
