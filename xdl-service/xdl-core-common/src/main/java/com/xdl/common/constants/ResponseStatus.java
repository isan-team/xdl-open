package com.xdl.common.constants;

/**
 * @ClassName Constants
 * @Description 通用请求返回封装
 * @Author iron
 * @Date 2020/8/10 10:59
 * @Version 1.0
 */
public enum ResponseStatus {

    RESPONSE_SUCCESS(20000, "请求成功"),
    RESPONSE_ERROR(52000, "请求失败"),
    ACCOUNT_DISABLED(53000, "该账户已注销，请联系管理员!"),
    ACCOUNT_LOCKED(54000,"该账号已被锁定，请联系管理员!"),
    ACCOUNT_EXPIRED(54000,"该账号已过期，请联系管理员!"),
    CREDENTIALS_EXPIRED(54000,"该账户的登录凭证已过期，请重新登录!"),
    JWTINFO_FAIL(55000,"解析tonken失败!"),
    USER_IS_NULL(56000,"该用户名不存在！"),
    PASSWORD_IS_ERROR(57000,"用户名或密码错误!"),
    USERNAME_PASSWORD_ISBLANK(58000,"用户名或密码为空！"),
    DICT_IS_NULL(59000,"字典值为空！"),
    UPDATE_ERROR(60000,"更新信息失败！"),
    LOGINCODE_ERROR(61000,"验证码配置信息错误!"),
    CODE_IS_INVALID(62000,"验证码无效！")
    ;


    /**
     * 内容
     */
    private Integer code;
    /**
     * name
     */
    private String value;

    /**
     * 构造方法
     */
    ResponseStatus(Integer code, String value) {
        this.code = code;
        this.value = value;
    }


    /**
     * 普通value查询name
     */
    public static String getName(Integer code) {
        for (ResponseStatus c : ResponseStatus.values()) {
            if (c.getCode().equals(code)) {
                return c.value;
            }
        }
        return null;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
