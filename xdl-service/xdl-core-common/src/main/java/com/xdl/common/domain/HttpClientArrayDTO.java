package com.xdl.common.domain;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.print.attribute.standard.MediaSize;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class HttpClientArrayDTO {

    private Integer code;

    private String msg;

    private JSONArray data;

    @JSONField(name = "total_results")
    private String total;

    @JSONField(name = "min_id")
    private Integer minId;

}
