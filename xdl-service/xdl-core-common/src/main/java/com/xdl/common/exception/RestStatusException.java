package com.xdl.common.exception;

import com.xdl.common.constants.ResponseStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @ClassName RestStatusException
 * @Description TODO
 * @Author iron
 * @Date 2020/8/27 11:27
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class RestStatusException extends RuntimeException{

    private static final long serialVersionUID = -8541311111016065562L;

    private String message;

    private Integer code;

    public RestStatusException(ResponseStatus status) {
    	this.code=status.getCode();
    	this.message=status.getValue();
    }

    public RestStatusException(String msg) {
        this.code=52000;
        this.message=msg;
    }

}