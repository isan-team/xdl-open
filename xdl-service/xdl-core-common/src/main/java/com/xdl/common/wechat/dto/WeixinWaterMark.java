package com.xdl.common.wechat.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 微信水印出来
 * @author iron guo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WeixinWaterMark {

    private LocalDateTime timestamp;

    private String appid;
}
