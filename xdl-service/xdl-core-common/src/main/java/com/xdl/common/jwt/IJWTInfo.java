package com.xdl.common.jwt;

public interface IJWTInfo {

    /**
     * 获取用户名
     * @return
     */
    String getUserName();

    /**
     * 获取用户ID
     * @return
     */
    String getUserId();

    String getOpenId();


    String getPhone();

    String getPassword();
}
