package com.xdl.common.response;

import com.xdl.common.constants.ResponseStatus;
import com.xdl.common.domain.HdkCollection;

import java.util.Collection;

public class ResponseHdkResult<T> {
    /**
     * 返回码
     */
    private Integer code;
    /**
     * 消息
     */
    private String msg;

    /**
     * 数据集合
     */
    private Collection<T> data;


    private Integer minId;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Collection<T> getData() {
        return data;
    }

    public void setData(Collection<T> data) {
        this.data = data;
    }

    public Integer getMinId() {
        return minId;
    }

    public void setMinId(Integer minId) {
        this.minId = minId;
    }

    public ResponseHdkResult() {}

    public ResponseHdkResult(Integer code, String msg, Collection<T> data,Integer minId) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.minId=minId;
    }

    // 通用封装
    public static <T> ResponseHdkResult<T>  setCollectionResult(Integer code, String msg, Collection<T> data,Integer minId) {
        return new ResponseHdkResult<T> (code, msg, data,minId);
    }

    public static  <T> ResponseHdkResult<T> Error(Integer code, String msg) {
        return setCollectionResult(code, msg, null,null);
    }

    // 返回错误，可以传msg
    public static  <T> ResponseHdkResult<T> Error(String msg) {
        return setCollectionResult(ResponseStatus.RESPONSE_ERROR.getCode(), msg, null,null);
    }

    // 返回错误
    public static  <T> ResponseHdkResult<T> Error() {
        return setCollectionResult(ResponseStatus.RESPONSE_ERROR.getCode(), ResponseStatus.RESPONSE_ERROR.getValue(), null,null);
    }

    // 返回成功，可以传data值
    public static  <T> ResponseHdkResult<T> Success(Collection<T> data,Integer minId) {
        return setCollectionResult(ResponseStatus.RESPONSE_SUCCESS.getCode(), ResponseStatus.RESPONSE_SUCCESS.getValue(), data,minId);
    }

    public static  <T> ResponseHdkResult<T> Success(HdkCollection<T> Object) {
        return setCollectionResult(ResponseStatus.RESPONSE_SUCCESS.getCode(), ResponseStatus.RESPONSE_SUCCESS.getValue(), Object.getData(),Object.getMinId());
    }


    // 返回成功，可以传data值
    public static  <T> ResponseHdkResult<T> Success(Collection<T> data) {
        return setCollectionResult(ResponseStatus.RESPONSE_SUCCESS.getCode(), ResponseStatus.RESPONSE_SUCCESS.getValue(), data,null);
    }

    // 返回成功，沒有data值
    public static  <T> ResponseHdkResult<T> Success() {
        return setCollectionResult(ResponseStatus.RESPONSE_SUCCESS.getCode(), ResponseStatus.RESPONSE_SUCCESS.getValue(), null,null);
    }

    // 返回成功，沒有data值
    public static  <T> ResponseHdkResult<T> Success(String msg) {
        return setCollectionResult(ResponseStatus.RESPONSE_SUCCESS.getCode(), msg, null,null);
    }
}
