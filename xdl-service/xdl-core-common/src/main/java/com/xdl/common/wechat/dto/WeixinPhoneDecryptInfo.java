package com.xdl.common.wechat.dto;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @description: 微信手机号信息解密后的对象
 * @author: iron guo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WeixinPhoneDecryptInfo {

    private String phoneNumber;

    private String purePhoneNumber;

    private int countryCode;

    private String watermark;

    private WeixinWaterMark weixinWaterMark;

    public void setWatermark(String watermark) {
        this.watermark = watermark;
        this.weixinWaterMark = JSON.toJavaObject(JSON.parseObject(this.watermark),WeixinWaterMark.class);
    }



}
