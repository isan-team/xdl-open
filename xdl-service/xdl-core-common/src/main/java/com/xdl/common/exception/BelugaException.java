package com.xdl.common.exception;

import com.xdl.common.constants.ResponseStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class BelugaException {

    private BelugaException() {
    }

    /**
     * 返回统一异常
     *
     * @param status
     */
    public static void fatal(ResponseStatus status) {
        log.error("return common error by {}", status.getValue());
        throw new RestStatusException(status);
    }

    /**
     * 返回统一异常
     *
     * @param msg
     */
    public static void fatal(String msg) {
        log.error("return common error by {}", msg);
        throw new RestStatusException(msg);
    }
}