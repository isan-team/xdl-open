package com.xdl.common.response;


import com.xdl.common.constants.ResponseStatus;

import java.util.Collection;
/**
 * @ClassName ResponseCollection
 * @Description TODO
 * @Author iron
 * @Date 2020/9/10 14:25
 * @Version 1.0
 */
public class ResponseCollection<T> {
    /**
     * 返回码
     */
    private Integer code;
    /**
     * 消息
     */
    private String msg;

    /**
     * 数据集合
     */
    private Collection<T> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Collection<T> getData() {
        return data;
    }

    public void setData(Collection<T> data) {
        this.data = data;
    }

    public ResponseCollection() {}

    public ResponseCollection(Integer code, String msg, Collection<T> data) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    // 通用封装
    public static <T> ResponseCollection<T>  setCollectionResult(Integer code, String msg, Collection<T> data) {
        return new ResponseCollection<T> (code, msg, data);
    }

    public static  <T> ResponseCollection<T> Error(Integer code, String msg) {
        return setCollectionResult(code, msg, null);
    }

    // 返回错误，可以传msg
    public static  <T> ResponseCollection<T> Error(String msg) {
        return setCollectionResult(ResponseStatus.RESPONSE_ERROR.getCode(), msg, null);
    }

    // 返回错误
    public static  <T> ResponseCollection<T> Error() {
        return setCollectionResult(ResponseStatus.RESPONSE_ERROR.getCode(), ResponseStatus.RESPONSE_ERROR.getValue(), null);
    }

    // 返回成功，可以传data值
    public static  <T> ResponseCollection<T> Success(Collection<T> data) {
        return setCollectionResult(ResponseStatus.RESPONSE_SUCCESS.getCode(), ResponseStatus.RESPONSE_SUCCESS.getValue(), data);
    }

    // 返回成功，沒有data值
    public static  <T> ResponseCollection<T> Success() {
        return setCollectionResult(ResponseStatus.RESPONSE_SUCCESS.getCode(), ResponseStatus.RESPONSE_SUCCESS.getValue(), null);
    }

    // 返回成功，沒有data值
    public static  <T> ResponseCollection<T> Success(String msg) {
        return setCollectionResult(ResponseStatus.RESPONSE_SUCCESS.getCode(), msg, null);
    }
}