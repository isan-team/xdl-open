package com.xdl.common.domain;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SuperClassifyDto {

    private Integer code;

    private String msg;

    @JSONField(name = "general_classify")
    private JSONArray data;

    @JSONField(name = "min_id")
    private Integer minId;

}
